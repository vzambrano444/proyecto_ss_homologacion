var express = require('express');
var app = express.Router();
var passport = require('passport');
//se instancia los diferentes controladores con sus respectivos modelos
var cuenta = require('../controllers/controladorCuenta');
var controladorCuenta = new cuenta();
var persona = require('../controllers/controladorPersona');
var controladorPersona = new persona();
var docente = require('../controllers/controladorDocente');
var controladorDocente = new docente();
var malla = require('../controllers/controladorMalla');
var controladorMalla = new malla();
var periodo = require('../controllers/controladorPeriodo');
var controladorPeriodo = new periodo();
var silabos = require('../controllers/controladorSilabos');
var controladorSilabos = new silabos();
var seguimiento = require('../controllers/controladorSeguimiento');
var controladorseguimiento = new seguimiento();
var solicitudes = require('../controllers/controladorSolicitudes');
var controladorsolicitudes = new solicitudes();
var tramite = require('../controllers/controladorTramite');
var controladorTramite = new tramite();
var asignacion = require('../controllers/controladorAsignacion');
var controladorAsignacion = new asignacion();
var PlanEstudio = require('../controllers/controladorPlanEstudios');
var controladorPlanEstudios = new PlanEstudio();
var sellos = require('../controllers/controladorSellos');
var controladorSellos = new sellos();
var contenido = require('../controllers/controladorRevContenido');
var controladorRevContenido = new contenido();
/**
 * Middleware de Autentificacion
 *
 */
var auth = function  middleware(req, res, next) {
    if (req.isAuthenticated()) {
        next();
    } else {
        // req.flash('err', 'Inicie sesion primero');
        res.redirect('/');
    }
};
/**
 * Middleware de Autentificacion Decano
 *
 */
var decano = function  middleware(req, res, next) {
    if (req.user.rolN === 'Decano') {
        next();
    } else {

        req.flash('error', 'No tiene acceso'),
                res.redirect('/');
    }
};
/**
 * Middleware de Autentificacion Abogado
 *
 */
var abogado = function  middleware(req, res, next) {
    if (req.user.rolN === 'Abogado') {
        next();
    } else {

        req.flash('error', 'No tiene acceso'),
                res.redirect('/');
    }
};
/**
 * Middleware de Autentificacion Docente
 *
 */
var docente = function  middleware(req, res, next) {
    if (req.user.rolN === 'Docente') {
        next();
    } else {

        req.flash('error', 'No tiene acceso'),
                res.redirect('/');
    }
};
/**
 * Middleware de Autentificacion Solicitante
 *
 */
var solicitante = function  middleware(req, res, next) {
    if (req.user.rolN === 'Solicitante') {
        next();
    } else {

        req.flash('error', 'No tiene acceso'),
                res.redirect('/');
    }
};
/**
 * Pantalla principal
 *
 * @url /login
 */
app.get('/', function (req, res, next) {
    res.redirect('/login');
});

////////////////////////Controlador Cuenta////////////////////////////////////
/**
 * Middleware Inicio de Sesion
 *
 * @section Usuario
 * @type post
 * @url /inicioSesion/entrar
 */
app.post('/inicioSesion/entrar',
        passport.authenticate('local-signin', {
            successRedirect: '/perfil',
            failureRedirect: '/'}
        ));
/**
 * Registro Cuenta
 *
 * @section Cuenta
 * @type post
 * @url /registro/guardar
 */
app.post('/registro/guardar',
        passport.authenticate('local-signup', {
            successRedirect: '/login',
            failureRedirect: '/registro'}
        ));
/**
 * Usuario unico 
 *
 * @section Cuenta
 * @type get
 * @url /registro/:texto
 */
app.get('/registro/:texto', controladorCuenta.CedulaUnica);
/**
 * Cerrar Cuenta
 *
 * @section Cuenta
 * @type get
 * @url /cerrar
 */
app.get('/cerrarSesion', auth, controladorCuenta.cerrar);
/**
 * Perfil del Usuario
 *
 * @section Usuario
 * @type get
 * @url /perfil
 */
app.get('/perfil', auth, controladorCuenta.verPerfil);
/**
 * Inicio de Sesion
 *
 * @section cuenta
 * @type get
 * @url /login
 */
app.get('/login', controladorCuenta.verLogin);
/**
 * Registro de Usuarios
 *
 * @section cuenta
 * @type get
 * @url /registro
 */
app.get('/registro', controladorCuenta.verRegistro);
/**
 * Lista de Usuarios
 *
 * @section cuenta
 * @type get
 * @url /lista/registro
 */
app.get('/lista/registro', decano, auth, controladorCuenta.verCuenta);
/**
 * Estado de cuenta
 *
 * @section cuenta
 * @type post
 * @url /estado/cuenta
 */
app.post('/estado/cuenta', decano, auth, controladorCuenta.bloquearCuenta);
/**
 * Rol de cuenta
 *
 * @section cuenta
 * @type post
 * @url /rol/cuenta
 */
app.post('/rol/cuenta', decano, auth, controladorCuenta.rolCuenta);
/**
 * Lista de Perfiles
 *
 * @section cuenta
 * @type get
 * @url /perfil/usuario
 */
app.get('/perfil/usuario', auth, controladorCuenta.ListarPerfil);
/**
 * Foto de Usuario
 *
 * @section cuenta
 * @type post
 * @url /perfil/foto
 */

app.post("/perfil/foto", auth, controladorCuenta.guardarFoto);



/**
 * Mostrar Malla 
 *
 * @section mallaCurricular
 * @type get
 * @url /menu/malla
 */
app.get('/menu/malla', auth, controladorMalla.VisualizarMalla);
/**
 * Editar Perfil
 *
 * @section persona
 * @type post
 * @url /perfil/editar
 */
app.post("/perfil/editar", auth, controladorPersona.editarPerfil);

/**
 * listar Docente
 *
 * @section docente
 * @type get
 * @url /lista/docente
 */
app.get("/lista/docente", decano, auth, controladorDocente.listarDocente);
/**
 * listar Docente API
 *
 * @section docente
 * @type get
 * @url /lista/docente/API/:texto
 */
app.get("/lista/docente/API/:texto", decano, auth, controladorDocente.buscarDocente);
/**
 * Guardar Docente
 *
 * @section docente
 * @type post
 * @url /guardar/docente
 */
app.post("/guardar/docente", decano, auth, controladorDocente.guardarDocente);
/**
 * Cargar Docente API
 *
 * @section docente
 * @type get
 * @url /cargar/docente/API/:texto
 */
app.get("/cargar/docente/API/:texto", decano, auth, controladorDocente.cargarDocente);

/**
 * listar Periodos
 *
 * @section periodo
 * @type get
 * @url /lista/periodo
 */
app.get("/lista/periodo", decano, auth, controladorPeriodo.listarPerido);
/**
 * listar Malla API
 *
 * @section periodo
 * @type get
 * @url /lista/periodo
 */
app.get("/lista/periodo/malla/", decano, auth, controladorPeriodo.listarMallaAPI);
/**
 * Guardar Periodo Academico
 *
 * @section periodo
 * @type post
 * @url /guardar/periodo
 */
app.post("/guardar/periodo", decano, auth, controladorPeriodo.GuardarPeriodo);
/**
 * DAR de Baja al Periodo Academico
 *
 * @section periodo
 * @type post
 * @url /bloquear/periodo
 */
app.post("/bloquear/periodo", decano, auth, controladorPeriodo.bloquearPeriodo);
/**
 * listar Silabos Academicos
 *
 * @section silabos
 * @type get
 * @url /bloquear/periodo
 */
app.get("/lista/silabos", controladorSilabos.listarSilabos);
/**
 * Cargar Silabos
 *
 * @section silabos
 * @type get
 * @url /guardar/silabos/
 */
app.post("/guardar/silabos/", auth, controladorSilabos.guardarSilabos);
/**
 * carga de  Silabos API
 *
 * @section silabos
 * @type get
 * @url /lista/silabos/API/
 */
app.get("/lista/silabos/API/:texto", auth, controladorSilabos.silabosAPI);
/**
 * Editar carga de  Silabos
 *
 * @section silabos
 * @type get
 * @url /editar/silabos/
 */
app.post("/editar/silabos/", auth, controladorSilabos.editarSilabos);
/**
 * Editar estado de  Silabos
 *
 * @section silabos
 * @type get
 * @url /estado/silabos/
 */
app.post("/estado/silabos/", auth, controladorSilabos.bloquearSilabo);
/**
 * Visualizar seguimiento
 *
 * @section seguimiento
 * @type get
 * @url //menu/seguimiento
 */
app.get("/menu/seguimiento", auth, controladorseguimiento.visualizarSeguimiento);

/**
 * Visualizar solicitudes
 *
 * @section solicitudes
 * @type get
 * @url /menu/solicitudes
 */
app.get("/menu/solicitudes", decano, auth, controladorsolicitudes.visualizarSolicitudes);
/**
 * Sumilar solicitudes
 *
 * @section solicitudes
 * @type post
 * @url /sumillar/solicitudes
 */
app.post("/sumillar/solicitudes", auth, controladorsolicitudes.sumillarSolicitud);
/**
 * Visualizar tramite
 *
 * @section tramites
 * @type get
 * @url /menu/tramite
 */
app.get("/menu/tramite", auth, controladorTramite.visualizarTramite);
/**
 * Guardar tramite
 *
 * @section tramites
 * @type post
 * @url /guardar/tramite
 */
app.post("/guardar/tramite", auth, controladorTramite.guardarTramite);
/**
 * Blloquear tramite
 *
 * @section tramites
 * @type post
 * @url /bloquear/tramite
 */
app.post("/bloquear/tramite", auth, controladorTramite.bloquearTramite);
/**
 * listarAsiganacion Docente
 *
 * @section Asignaciones
 * @type get
 * @url /listar/asignacion
 */
app.get("/listar/asignacion", decano, auth, controladorAsignacion.visualizarAsignaciones);
/**
 * Cargar Docente API
 *
 * @section Asignaciones
 * @type get
 * @url /cargar/docente/API2/:texto
 */
app.get("/cargar/docente/API2/:texto", auth, controladorAsignacion.cargarDocente);
/**
 * Asiganar  Docente 
 *
 * @section Asignaciones
 * @type post
 * @url /asignar/docente
 */
app.post("/asignar/docente", auth, controladorAsignacion.asignarDocente);

/**
 * Mostrar Plan de Estudios
 *
 * @section mallaCurricular
 * @type get
 * @url /menu/plan
 */
app.get('/menu/plan', auth, controladorPlanEstudios.listarPlan);


/**
 * Revisar Sellos 
 *
 * @section 
 * @type post
 * @url /revision/sellos
 */
app.post("/revision/sellos", auth, controladorSellos.revisionSellos);
/**
 * Mostrar requisitos 
 *
 * @section 
 * @type get
 * @url /listar/requisitos
 */
app.get("/listar/requisitos", auth, function (req, res, next) {
    if (req.user.rolN === 'Decano') {
        res.render('partials/decano/index',
                {titulo: "Sistema de Homologacion",
                    partial: 'decano/frm_requisitos',
                    usuario: cuenta,
                    correo: req.user.correo,
                    foto: req.user.foto,
                    nombre: req.user.nom,
                    rol: req.user.rolN,
                    id_persona: req.user.id_persona,
                });
    } else if (req.user.rolN === 'Abogado') {
        res.render('partials/abogado/index',
                {titulo: "Sistema de Homologacion",
                    partial: 'abogado/frm_requisitos',
                    usuario: cuenta,
                    correo: req.user.correo,
                    foto: req.user.foto,
                    nombre: req.user.nom,
                    rol: req.user.rolN,
                    id_persona: req.user.id_persona,
                });
    } else if (req.user.rolN === 'Docente') {
        res.render('partials/docente/index',
                {titulo: "Sistema de Homologacion",
                    partial: 'docente/frm_requisitos',
                    usuario: cuenta,
                    correo: req.user.correo,
                    foto: req.user.foto,
                    nombre: req.user.nom,
                    rol: req.user.rolN,
                    id_persona: req.user.id_persona,
                });
    } else if (req.user.rolN === 'Solicitante') {
        res.render('partials/solicitante/index',
                {titulo: "Sistema de Homologacion",
                    partial: 'solicitante/frm_requisitos',
                    usuario: cuenta,
                    correo: req.user.correo,
                    foto: req.user.foto,
                    nombre: req.user.nom,
                    rol: req.user.rolN,
                    id_persona: req.user.id_persona,
                });
    }

});
/**
 * Revisar Contenido
 *
 * @section Revision de contenido
 * @type post
 * @url /revision/sellos
 */
app.post("/revision/contenido", auth, controladorRevContenido.revisionContenido);
/**
 * Revisar Contenido Estado
 *
 * @section Revision de contenido
 * @type post
 * @url /revision/estado
 */
app.post("/revision/estado", auth, controladorRevContenido.editar);
module.exports = app;

    