'use strict';
var models = require('../models/');
var Persona = models.persona;
/**
 * Clase que permite manipular los datos del modelo persona
 */
class controladorPersona {
    /**
     * Funcion que permite editar el perfil de la persona autentificada
     * @param {type} req el objeto peticion
     * @param {type} res el objeto respuesta
     * @returns {undefined} redirecciona a la pagina /perfil/usuario
     */
    editarPerfil(req, res) {
        Persona.update({
            apellidos: req.body.apellidos,
            nombres: req.body.nombres,
            fecha_nacimiento: req.body.fecha_nacimiento,
            edad: req.body.edad,
            direccion: req.body.direccion,
            telefono: req.body.telefono
        }, {where: {external_id: req.body.external}}).then(function (updatedPersona, created) {
            if (updatedPersona) {
                req.flash('info', 'Se ha modificado correctamente', false);
                res.redirect('/perfil/usuario');
            }
        });

    }

}


module.exports = controladorPersona;
