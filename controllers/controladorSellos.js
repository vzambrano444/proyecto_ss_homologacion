'use strict';
var models = require('../models/');
var Seguimiento = models.seguimiento;
/**
 * Clase que permite manipular los datos del modelo seguimiento
 */
class controladorSellos {
    /**
     * Funcion que permite modificar el estado para decir si estan o no completos los sellos
     * @param {type} req el objeto peticion
     * @param {type} res el objeto respuesta
     * @returns {undefined} redirecciona a la pagina /menu/tramite
     */
    revisionSellos(req, res) {
        Seguimiento.update({
            revSellos: req.body.estado
        }, {where: {external_id: req.body.external}}).then(function (updatedSeguimiento, created) {
            if (updatedSeguimiento) {
                req.flash('info', 'Operacion exitosa');
                res.redirect('/menu/tramite');

            }
        });
    }
}

module.exports = controladorSellos;

