'use strict';
var models = require('../models/');
const uuidv4 = require('uuid/v4');
var Periodo = models.periodoAcademico;
var Malla = models.mallaCurricular;
const Sequelize = require('sequelize');
/**
 * sincronizacion a base de datos
 */
const sequelize = new Sequelize('SistemaHomologacion', 'root', '12345', {
    host: 'localhost',
    dialect: 'mysql',
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
    storage: 'path/to/database.sqlite',
    operatorsAliases: false
});
/**
 * Clase que permite manipular los datos del modelo malla, periodo
 */
class controladorPerido {
    /**
     * Funcion que permite mostrar la vista para visualizar los periodos academicos
     * @param {type} req el objeto peticion
     * @param {type} res el objeto respuesta
     * @returns {undefined} redirecciona a la pagina /lista/periodo
     */
    listarPerido(req, res) {
        Periodo.findAll({}).then(function (periodos) {
            sequelize.query("select * from mallaCurricular,periodoAcademico where periodoAcademico.external_malla = mallaCurricular.external_id",
                    {type: sequelize.QueryTypes.SELECT}).then(function (periodo) {
                if (periodo) {
                    res.render('partials/decano/index',
                            {titulo: "Periodo",
                                partial: 'decano/frm_periodo',
                                Periodo: periodo,
                                nombre: req.user.nom,
                                rol: req.user.rolN,
                                foto: req.user.foto,
                                msg: {
                                    error: req.flash('error'),
                                    info: req.flash("info")
                                }
                            });
                }
            });
        }).catch(function (err) {
            req.flash('info', 'Hubo un error');
            res.redirect('/');
        });
    }
    /**
     * Funcion que permite buscar las mallas en estado activo
     * @param {type} req el objeto peticion
     * @param {type} res el objeto respuesta
     * @returns {undefined} retorna un json con las masllas een estado activo
     */
    listarMallaAPI(req, res) {
        Malla.findAll({where: {estado: "1"}}).then(function (malla) {
            res.status(200).json(malla);
        });
    }
    /**
     * Funcion que permite guardar los periodos academicos
     * @param {type} req el objeto peticion
     * @param {type} res el objeto respuesta
     * @returns {undefined} redirecciona a la pagina /lista/periodo
     */
    GuardarPeriodo(req, res) {
        if (req.body.external === "0") {
            Periodo.create({
                fechaInicio: req.body.fecha_inicio,
                fechaFin: req.body.fecha_fin,
                external_malla: req.body.malla,
                estado: true,
                external_id: uuidv4()
            }).then(function (newPeriodo, created) {
                if (newPeriodo) {
                    req.flash('info', 'Se a creado con exito');
                    res.redirect('/lista/periodo');
                }
            });

        } else {
            Periodo.update({
                fechaInicio: req.body.fecha_inicio,
                fechaFin: req.body.fecha_fin,
                external_malla: req.body.malla
            }, {where: {external_id: req.body.external}}).then(function (updatedPeriodo, created) {
                if (updatedPeriodo) {
                    req.flash('info', 'Se a modificado con exito');
                    res.redirect('/lista/periodo');
                }
            });
        }

    }
    /**
     * Funcion que permite dar de baja a los periodos academicos ya trascurridos
     * @param {type} req el objeto peticion
     * @param {type} res el objeto respuesta
     * @returns {undefined} redirecciona a la pagina /lista/periodo
     */
    bloquearPeriodo(req, res) {
        Periodo.update({estado: req.body.estado}, {where: {external_id: req.body.externalE}}).then(function (updatedPeriodo, created) {
            if (updatedPeriodo) {
                req.flash('info', 'Se a modificado con exito');
                res.redirect('/lista/periodo');
            }
        });
    }

}


module.exports = controladorPerido;
