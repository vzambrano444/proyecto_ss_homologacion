var models = require('../models/');
const uuidv4 = require('uuid/v4');
var bCrypt = require('bcrypt-nodejs');
var Rol = models.rol;
var Cuenta = models.cuenta;
var Persona = models.persona;
var Carrera = models.carrera;
var Malla = models.mallaCurricular;
var Ciclo = models.ciclo;
var Materia = models.materia;
var Docente = models.docente;
/**
 * Clase que permite manipular los datos del modelo rol, cuenta,persona, carrera , malla, ciclo, materia, docente
 * para generar datos automaticamente
 */
///////////////ROL////////////
var init = function () {
    Rol.findOrCreate({where: {nombre: 'Decano'}, defaults: {id: "1", nombre: 'Decano'}}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });
    Rol.findOrCreate({where: {nombre: 'Abogado'}, defaults: {id: "2", nombre: 'Abogado'}}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });
    Rol.findOrCreate({where: {nombre: 'Docente'}, defaults: {id: "3", nombre: 'Docente'}}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });
    Rol.findOrCreate({where: {nombre: 'Solicitante'}, defaults: {id: "4", nombre: 'Solicitante'}}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });
//////////////////////////PERSONA//////////////////////////
/////////////Decano//////////
    Persona.findOrCreate({where: {id_rol: '1', nombres: 'Josue Andres'}, defaults: {
            id: "1",
            edad: "24",
            cedula: '1104123425',
            nombres: 'Josue Andres',
            apellidos: 'Macas Caraguay',
            telefono: '0999999999',
            direccion: 'Loja',
            foto: 'sin_foto.jpg',
            fecha_nacimiento: '1995-07-06',
            external_id: uuidv4(),
            id_rol: '1'
        }}).spread((user, persona) => {
        var clave = bCrypt.hashSync('1234', bCrypt.genSaltSync(8), null);
        Cuenta.findOrCreate({where: {id_persona: '1', correo: 'admin@unl.edu.ec'}, defaults: {
                id: "1",
                correo: 'admin@unl.edu.ec',
                clave: clave,
                external_id: uuidv4(),
                estado: true,
                id_persona: '1'
            }}).spread((user_cuenta, cuenta) => {
            console.log("Se inserto persona y cuenta del decano");
        });
    });
    /////////////Abogado//////////
    Persona.findOrCreate({where: {id_rol: '2', nombres: 'Edmundo Jose'}, defaults: {
            id: "2",
            edad: "24",
            cedula: '9999999999',
            nombres: 'Edmundo Jose',
            apellidos: 'Pezantes Urrego',
            telefono: '0999988888',
            direccion: 'Loja',
            foto: 'sin_foto.jpg',
            fecha_nacimiento: '1995-07-12',
            external_id: uuidv4(),
            id_rol: '2'
        }}).spread((user, persona) => {
        var clave = bCrypt.hashSync('1234', bCrypt.genSaltSync(8), null);
        Cuenta.findOrCreate({where: {id_persona: '2', correo: 'abogado@unl.edu.ec'}, defaults: {
                id: "2",
                correo: 'abogado@unl.edu.ec',
                clave: clave,
                external_id: uuidv4(),
                estado: true,
                id_persona: '2'
            }}).spread((user_cuenta, cuenta) => {
            console.log("Se inserto persona y cuenta del abogado");
        });
    });
    /////////////Docentes//////////
    Persona.findOrCreate({where: {id_rol: '3', nombres: 'Jorge Ivan'}, defaults: {
            id: "3",
            edad: "65",
            cedula: '1103578355',
            nombres: 'Jorge Ivan',
            apellidos: 'Tocto',
            telefono: '0987654321',
            direccion: 'Loja',
            foto: 'sin_foto.jpg',
            fecha_nacimiento: '1954-01-12',
            external_id: uuidv4(),
            id_rol: '3'
        }}).spread((user, persona) => {
        var clave = bCrypt.hashSync('1234', bCrypt.genSaltSync(8), null);
        Cuenta.findOrCreate({where: {id_persona: '3', correo: 'ing.jorge.tocto@unl.edu.ec'}, defaults: {
                id: "3",
                correo: 'ing.jorge.tocto@unl.edu.ec',
                clave: clave,
                external_id: uuidv4(),
                estado: true,
                id_persona: '3'
            }}).spread((user_cuenta, cuenta) => {
            console.log("Se inserto persona y cuenta del docente");
        });
    });
    Persona.findOrCreate({where: {id_rol: '3', nombres: 'Hernan'}, defaults: {
            id: "4",
            edad: "52",
            cedula: '1103213854',
            nombres: 'Hernan',
            apellidos: 'Torres Carrion',
            telefono: '0987654321',
            direccion: 'Loja',
            foto: 'sin_foto.jpg',
            fecha_nacimiento: '1967-02-12',
            external_id: uuidv4(),
            id_rol: '3'
        }}).spread((user, persona) => {
        var clave = bCrypt.hashSync('1234', bCrypt.genSaltSync(8), null);
        Cuenta.findOrCreate({where: {id_persona: '4', correo: 'ing.hernan.torres@unl.edu.ec'}, defaults: {
                id: "4",
                correo: 'ing.hernan.torres@unl.edu.ec',
                clave: clave,
                external_id: uuidv4(),
                estado: true,
                id_persona: '4'
            }}).spread((user_cuenta, cuenta) => {
            console.log("Se inserto persona y cuenta del docente");
        });
    });
    Persona.findOrCreate({where: {id_rol: '3', nombres: 'Mario'}, defaults: {
            id: "5",
            edad: "39",
            cedula: '1101563912',
            nombres: 'Mario',
            apellidos: 'Cueva Hurtado',
            telefono: '0987654321',
            direccion: 'Loja',
            foto: 'sin_foto.jpg',
            fecha_nacimiento: '1980-05-02',
            external_id: uuidv4(),
            id_rol: '3'
        }}).spread((user, persona) => {
        var clave = bCrypt.hashSync('1234', bCrypt.genSaltSync(8), null);
        Cuenta.findOrCreate({where: {id_persona: '5', correo: 'ing.mario.cueva@unl.edu.ec'}, defaults: {
                id: "5",
                correo: 'ing.mario.cueva@unl.edu.ec',
                clave: clave,
                external_id: uuidv4(),
                estado: true,
                id_persona: '5'
            }}).spread((user_cuenta, cuenta) => {
            console.log("Se inserto persona y cuenta del docente");
        });
    });
    Persona.findOrCreate({where: {id_rol: '3', nombres: 'Edison'}, defaults: {
            id: "6",
            edad: "37",
            cedula: '1102002852',
            nombres: 'Edison',
            apellidos: 'Coronel Romero',
            telefono: '0987654321',
            direccion: 'Loja',
            foto: 'sin_foto.jpg',
            fecha_nacimiento: '1981-06-12',
            external_id: uuidv4(),
            id_rol: '3'
        }}).spread((user, persona) => {
        var clave = bCrypt.hashSync('1234', bCrypt.genSaltSync(8), null);
        Cuenta.findOrCreate({where: {id_persona: '6', correo: 'ing.edison.coronel@unl.edu.ec'}, defaults: {
                id: "6",
                correo: 'ing.edison.coronel@unl.edu.ec',
                clave: clave,
                external_id: uuidv4(),
                estado: true,
                id_persona: '6'
            }}).spread((user_cuenta, cuenta) => {
            console.log("Se inserto persona y cuenta del docente");
        });
    });
    Persona.findOrCreate({where: {id_rol: '3', nombres: 'Valeria'}, defaults: {
            id: "7",
            edad: "36",
            cedula: '1102477807',
            nombres: 'Valeria',
            apellidos: 'Herrera Salazar',
            telefono: '0987654321',
            direccion: 'Loja',
            foto: 'sin_foto.jpg',
            fecha_nacimiento: '1983-01-12',
            external_id: uuidv4(),
            id_rol: '3'
        }}).spread((user, persona) => {
        var clave = bCrypt.hashSync('1234', bCrypt.genSaltSync(8), null);
        Cuenta.findOrCreate({where: {id_persona: '7', correo: 'ing.valeria.herrera@unl.edu.ec'}, defaults: {
                id: "7",
                correo: 'ing.valeria.herrera@unl.edu.ec',
                clave: clave,
                external_id: uuidv4(),
                estado: true,
                id_persona: '7'
            }}).spread((user_cuenta, cuenta) => {
            console.log("Se inserto persona y cuenta del docente");
        });
    });
    Persona.findOrCreate({where: {id_rol: '3', nombres: 'Pablo'}, defaults: {
            id: "8",
            edad: "40",
            cedula: '1102913389',
            nombres: 'Pablo',
            apellidos: 'Ordoñez Ordoñez',
            telefono: '0987654321',
            direccion: 'Loja',
            foto: 'sin_foto.jpg',
            fecha_nacimiento: '1979-01-12',
            external_id: uuidv4(),
            id_rol: '3'
        }}).spread((user, persona) => {
        var clave = bCrypt.hashSync('1234', bCrypt.genSaltSync(8), null);
        Cuenta.findOrCreate({where: {id_persona: '8', correo: 'ing.pablo.ordonez@unl.edu.ec'}, defaults: {
                id: "8",
                correo: 'ing.pablo.ordonezo@unl.edu.ec',
                clave: clave,
                external_id: uuidv4(),
                estado: true,
                id_persona: '8'
            }}).spread((user_cuenta, cuenta) => {
            console.log("Se inserto persona y cuenta del docente");
        });
    });
    Persona.findOrCreate({where: {id_rol: '3', nombres: 'Luis'}, defaults: {
            id: "9",
            edad: "45",
            cedula: '1101808317',
            nombres: 'Luis',
            apellidos: 'Chamba Eras',
            telefono: '0987654321',
            direccion: 'Loja',
            foto: 'sin_foto.jpg',
            fecha_nacimiento: '1974-01-12',
            external_id: uuidv4(),
            id_rol: '3'
        }}).spread((user, persona) => {
        var clave = bCrypt.hashSync('1234', bCrypt.genSaltSync(8), null);
        Cuenta.findOrCreate({where: {id_persona: '9', correo: 'ing.luis.chamba@unl.edu.ec'}, defaults: {
                id: "9",
                correo: 'ing.luis.chamba@unl.edu.ec',
                clave: clave,
                external_id: uuidv4(),
                estado: true,
                id_persona: '9'
            }}).spread((user_cuenta, cuenta) => {
            console.log("Se inserto persona y cuenta del docente");
        });
    });
    Persona.findOrCreate({where: {id_rol: '3', nombres: 'Fransisco Javier'}, defaults: {
            id: "10",
            edad: "42",
            cedula: '1103027650',
            nombres: 'Fransisco Javier',
            apellidos: 'Alvarez Pineda',
            telefono: '0987654321',
            direccion: 'Loja',
            foto: 'sin_foto.jpg',
            fecha_nacimiento: '1977-01-12',
            external_id: uuidv4(),
            id_rol: '3'
        }}).spread((user, persona) => {
        var clave = bCrypt.hashSync('1234', bCrypt.genSaltSync(8), null);
        Cuenta.findOrCreate({where: {id_persona: '10', correo: 'ing.fransisco.alvarez@unl.edu.ec'}, defaults: {
                id: "10",
                correo: 'ing.fransisco.alvarez@unl.edu.ec',
                clave: clave,
                external_id: uuidv4(),
                estado: true,
                id_persona: '10'
            }}).spread((user_cuenta, cuenta) => {
            console.log("Se inserto persona y cuenta del docente");
        });
    });
    Persona.findOrCreate({where: {id_rol: '3', nombres: 'Oscar Miguel'}, defaults: {
            id: "11",
            edad: "38",
            cedula: '1104571474',
            nombres: 'Oscar Miguel',
            apellidos: 'Cumbicus Pineda',
            telefono: '0987654321',
            direccion: 'Loja',
            foto: 'sin_foto.jpg',
            fecha_nacimiento: '1981-01-12',
            external_id: uuidv4(),
            id_rol: '3'
        }}).spread((user, persona) => {
        var clave = bCrypt.hashSync('1234', bCrypt.genSaltSync(8), null);
        Cuenta.findOrCreate({where: {id_persona: '11', correo: 'ing.oscar.cumbicus@unl.edu.ec'}, defaults: {
                id: "11",
                correo: 'ing.oscar.cumbicus@unl.edu.ec',
                clave: clave,
                external_id: uuidv4(),
                estado: true,
                id_persona: '11'
            }}).spread((user_cuenta, cuenta) => {
            console.log("Se inserto persona y cuenta del docente");
        });
    });
    Persona.findOrCreate({where: {id_rol: '3', nombres: 'Gaston Rene'}, defaults: {
            id: "12",
            edad: "37",
            cedula: '1102731468',
            nombres: 'Gaston Rene',
            apellidos: 'Chamba Romero',
            telefono: '0987654321',
            direccion: 'Loja',
            foto: 'sin_foto.jpg',
            fecha_nacimiento: '1982-01-12',
            external_id: uuidv4(),
            id_rol: '3'
        }}).spread((user, persona) => {
        var clave = bCrypt.hashSync('1234', bCrypt.genSaltSync(8), null);
        Cuenta.findOrCreate({where: {id_persona: '12', correo: 'ing.gaston.chamba@unl.edu.ec'}, defaults: {
                id: "12",
                correo: 'ing.gaston.chamba@unl.edu.ec',
                clave: clave,
                external_id: uuidv4(),
                estado: true,
                id_persona: '12'
            }}).spread((user_cuenta, cuenta) => {
            console.log("Se inserto persona y cuenta del docente");
        });
    });
    Persona.findOrCreate({where: {id_rol: '3', nombres: 'Wilman Patricio'}, defaults: {
            id: "13",
            edad: "40",
            cedula: '1102071428',
            nombres: 'Wilman Patricio',
            apellidos: 'Chamba Zaragocin',
            telefono: '0987654321',
            direccion: 'Loja',
            foto: 'sin_foto.jpg',
            fecha_nacimiento: '1979-01-12',
            external_id: uuidv4(),
            id_rol: '3'
        }}).spread((user, persona) => {
        var clave = bCrypt.hashSync('1234', bCrypt.genSaltSync(8), null);
        Cuenta.findOrCreate({where: {id_persona: '13', correo: 'ing.wilman.chamba@unl.edu.ec'}, defaults: {
                id: "13",
                correo: 'ing.wilman.chamba@unl.edu.ec',
                clave: clave,
                external_id: uuidv4(),
                estado: true,
                id_persona: '13'
            }}).spread((user_cuenta, cuenta) => {
            console.log("Se inserto persona y cuenta del docente");
        });
    });
    Persona.findOrCreate({where: {id_rol: '3', nombres: 'Roberth Gustavo'}, defaults: {
            id: "14",
            edad: "38",
            cedula: '1103699268',
            nombres: 'Roberth Gustavo',
            apellidos: 'Figueroa Diaz',
            telefono: '0987654321',
            direccion: 'Loja',
            foto: 'sin_foto.jpg',
            fecha_nacimiento: '1981-01-12',
            external_id: uuidv4(),
            id_rol: '3'
        }}).spread((user, persona) => {
        var clave = bCrypt.hashSync('1234', bCrypt.genSaltSync(8), null);
        Cuenta.findOrCreate({where: {id_persona: '14', correo: 'ing.roberth.figueroa@unl.edu.ec'}, defaults: {
                id: "14",
                correo: 'ing.roberth.figueroa@unl.edu.ec',
                clave: clave,
                external_id: uuidv4(),
                estado: true,
                id_persona: '14'
            }}).spread((user_cuenta, cuenta) => {
            console.log("Se inserto persona y cuenta del docente");
        });
    });
    Persona.findOrCreate({where: {id_rol: '3', nombres: 'Angel Freddy'}, defaults: {
            id: "15",
            edad: "37",
            cedula: '1001226914',
            nombres: 'Angel Freddy',
            apellidos: 'Ganazhapa Malla',
            telefono: '0987654321',
            direccion: 'Loja',
            foto: 'sin_foto.jpg',
            fecha_nacimiento: '1980-01-12',
            external_id: uuidv4(),
            id_rol: '3'
        }}).spread((user, persona) => {
        var clave = bCrypt.hashSync('1234', bCrypt.genSaltSync(8), null);
        Cuenta.findOrCreate({where: {id_persona: '15', correo: 'ing.angel.ganazhapa@unl.edu.ec'}, defaults: {
                id: "15",
                correo: 'ing.angel.ganazhapa@unl.edu.ec',
                clave: clave,
                external_id: uuidv4(),
                estado: true,
                id_persona: '15'
            }}).spread((user_cuenta, cuenta) => {
            console.log("Se inserto persona y cuenta del docente");
        });
    });
    Persona.findOrCreate({where: {id_rol: '3', nombres: 'Edwin Rene'}, defaults: {
            id: "16",
            edad: "40",
            cedula: '1102000575',
            nombres: 'Edwin Rene',
            apellidos: 'Guaman Quinche',
            telefono: '0987654321',
            direccion: 'Loja',
            foto: 'sin_foto.jpg',
            fecha_nacimiento: '1979-01-12',
            external_id: uuidv4(),
            id_rol: '3'
        }}).spread((user, persona) => {
        var clave = bCrypt.hashSync('1234', bCrypt.genSaltSync(8), null);
        Cuenta.findOrCreate({where: {id_persona: '16', correo: 'ing.edwin.guaman@unl.edu.ec'}, defaults: {
                id: "16",
                correo: 'ing.edwin.guaman@unl.edu.ec',
                clave: clave,
                external_id: uuidv4(),
                estado: true,
                id_persona: '16'
            }}).spread((user_cuenta, cuenta) => {
            console.log("Se inserto persona y cuenta del docente");
        });
    });
    Persona.findOrCreate({where: {id_rol: '3', nombres: 'Jose Oswaldo'}, defaults: {
            id: "17",
            edad: "38",
            cedula: '1103056287',
            nombres: 'Jose Oswaldo',
            apellidos: 'Guaman Quinche',
            telefono: '0987654321',
            direccion: 'Loja',
            foto: 'sin_foto.jpg',
            fecha_nacimiento: '1981-01-12',
            external_id: uuidv4(),
            id_rol: '3'
        }}).spread((user, persona) => {
        var clave = bCrypt.hashSync('1234', bCrypt.genSaltSync(8), null);
        Cuenta.findOrCreate({where: {id_persona: '17', correo: 'ing.jose.guaman@unl.edu.ec'}, defaults: {
                id: "17",
                correo: 'ing.jose.guaman@unl.edu.ec',
                clave: clave,
                external_id: uuidv4(),
                estado: true,
                id_persona: '17'
            }}).spread((user_cuenta, cuenta) => {
            console.log("Se inserto persona y cuenta del docente");
        });
    });
    Persona.findOrCreate({where: {id_rol: '3', nombres: 'Ximena Yadira'}, defaults: {
            id: "18",
            edad: "38",
            cedula: '1100569530',
            nombres: 'Ximena Yadira',
            apellidos: 'Naranjo Ruiz',
            telefono: '0987654321',
            direccion: 'Loja',
            foto: 'sin_foto.jpg',
            fecha_nacimiento: '1981-01-12',
            external_id: uuidv4(),
            id_rol: '3'
        }}).spread((user, persona) => {
        var clave = bCrypt.hashSync('1234', bCrypt.genSaltSync(8), null);
        Cuenta.findOrCreate({where: {id_persona: '18', correo: 'ing.ximena.naranjo@unl.edu.ec'}, defaults: {
                id: "18",
                correo: 'ing.ximena.naranjo@unl.edu.ec',
                clave: clave,
                external_id: uuidv4(),
                estado: true,
                id_persona: '18'
            }}).spread((user_cuenta, cuenta) => {
            console.log("Se inserto persona y cuenta del docente");
        });
    });
    Persona.findOrCreate({where: {id_rol: '3', nombres: 'Cristian Ramiro'}, defaults: {
            id: "19",
            edad: "35",
            cedula: '1103379663',
            nombres: 'Cristian Ramiro',
            apellidos: 'Narvaez Guillen',
            telefono: '0987654321',
            direccion: 'Loja',
            foto: 'sin_foto.jpg',
            fecha_nacimiento: '1984-01-12',
            external_id: uuidv4(),
            id_rol: '3'
        }}).spread((user, persona) => {
        var clave = bCrypt.hashSync('1234', bCrypt.genSaltSync(8), null);
        Cuenta.findOrCreate({where: {id_persona: '19', correo: 'ing.cristian.narvaez@unl.edu.ec'}, defaults: {
                id: "19",
                correo: 'ing.cristian.narvaez@unl.edu.ec',
                clave: clave,
                external_id: uuidv4(),
                estado: true,
                id_persona: '19'
            }}).spread((user_cuenta, cuenta) => {
            console.log("Se inserto persona y cuenta del docente");
        });
    });
    Persona.findOrCreate({where: {id_rol: '3', nombres: 'Nora Esperanza'}, defaults: {
            id: "20",
            edad: "37",
            cedula: '1104889736',
            nombres: 'Nora Esperanza',
            apellidos: 'Parra Celi',
            telefono: '0987654321',
            direccion: 'Loja',
            foto: 'sin_foto.jpg',
            fecha_nacimiento: '1982-01-12',
            external_id: uuidv4(),
            id_rol: '3'
        }}).spread((user, persona) => {
        var clave = bCrypt.hashSync('1234', bCrypt.genSaltSync(8), null);
        Cuenta.findOrCreate({where: {id_persona: '20', correo: 'ing.nora.parra@unl.edu.ec'}, defaults: {
                id: "20",
                correo: 'ing.nora.parra@unl.edu.ec',
                clave: clave,
                external_id: uuidv4(),
                estado: true,
                id_persona: '20'
            }}).spread((user_cuenta, cuenta) => {
            console.log("Se inserto persona y cuenta del docente");
        });
    });
    Persona.findOrCreate({where: {id_rol: '3', nombres: 'Maria del Cisne'}, defaults: {
            id: "21",
            edad: "36",
            cedula: '1104956303',
            nombres: 'Maria del Cisne',
            apellidos: 'Ruilova Sanchez',
            telefono: '0987654321',
            direccion: 'Loja',
            foto: 'sin_foto.jpg',
            fecha_nacimiento: '1983-01-12',
            external_id: uuidv4(),
            id_rol: '3'
        }}).spread((user, persona) => {
        var clave = bCrypt.hashSync('1234', bCrypt.genSaltSync(8), null);
        Cuenta.findOrCreate({where: {id_persona: '21', correo: 'ing.maria.ruilova@unl.edu.ec'}, defaults: {
                id: "21",
                correo: 'ing.maria.ruilova@unl.edu.ec',
                clave: clave,
                external_id: uuidv4(),
                estado: true,
                id_persona: '21'
            }}).spread((user_cuenta, cuenta) => {
            console.log("Se inserto persona y cuenta del docente");
        });
    });
    Persona.findOrCreate({where: {id_rol: '3', nombres: 'Javier Fransisco'}, defaults: {
            id: "22",
            edad: "54",
            cedula: '1102750724',
            nombres: 'Javier Fransisco',
            apellidos: 'Sinche Freire',
            telefono: '0987654321',
            direccion: 'Loja',
            foto: 'sin_foto.jpg',
            fecha_nacimiento: '1965-01-12',
            external_id: uuidv4(),
            id_rol: '3'
        }}).spread((user, persona) => {
        var clave = bCrypt.hashSync('1234', bCrypt.genSaltSync(8), null);
        Cuenta.findOrCreate({where: {id_persona: '22', correo: 'ing.javier.sinche@unl.edu.ec'}, defaults: {
                id: "22",
                correo: 'ing.javier.sinche@unl.edu.ec',
                clave: clave,
                external_id: uuidv4(),
                estado: true,
                id_persona: '22'
            }}).spread((user_cuenta, cuenta) => {
            console.log("Se inserto persona y cuenta del docente");
        });
    });
    Persona.findOrCreate({where: {id_rol: '3', nombres: 'Marlon Santiago'}, defaults: {
            id: "23",
            edad: "43",
            cedula: '1102997077',
            nombres: 'Marlon Santiago',
            apellidos: 'Viñan Ludeña',
            telefono: '0987654321',
            direccion: 'Loja',
            foto: 'sin_foto.jpg',
            fecha_nacimiento: '1976-01-12',
            external_id: uuidv4(),
            id_rol: '3'
        }}).spread((user, persona) => {
        var clave = bCrypt.hashSync('1234', bCrypt.genSaltSync(8), null);
        Cuenta.findOrCreate({where: {id_persona: '23', correo: 'ing.marlon.vinan@unl.edu.ec'}, defaults: {
                id: "23",
                correo: 'ing.marlon.vinan@unl.edu.ec',
                clave: clave,
                external_id: uuidv4(),
                estado: true,
                id_persona: '23'
            }}).spread((user_cuenta, cuenta) => {
            console.log("Se inserto persona y cuenta del docente");
        });
    });
    Persona.findOrCreate({where: {id_rol: '3', nombres: 'Docente'}, defaults: {
            id: "24",
            edad: "99",
            cedula: '9999999999',
            nombres: 'Docente',
            apellidos: 'Docente',
            telefono: '0999999999',
            direccion: 'Docente',
            foto: 'sin_foto.jpg',
            fecha_nacimiento: '1111-11-11',
            external_id: "0",
            id_rol: '3'
        }}).spread((user, persona) => {
        var clave = bCrypt.hashSync('1234', bCrypt.genSaltSync(8), null);
        Cuenta.findOrCreate({where: {id_persona: '24', correo: 'Docente@unl.edu.ec'}, defaults: {
                id: "24",
                correo: 'Docente@unl.edu.ec',
                clave: clave,
                external_id: "0",
                estado: true,
                id_persona: '24'
            }}).spread((user_cuenta, cuenta) => {
            Docente.findOrCreate({where: {id_persona: '24'}, defaults: {
                    titulo: "0",
                    colaboracion: "0",
                    external_id: "0",
                    id_persona: '24'
                }}).spread((user_cuenta, cuenta) => {
                console.log("Se inserto persona y cuenta del docente");
            });
        });
    });

//////////////////CRERRERA///////////////////////
    Carrera.findOrCreate({where: {nombre: 'Ingieneria en Sistemas'}, defaults: {id: "1", nombre: 'Ingieneria en Sistemas', area: "Area de la energia, las industrias y los recursos no renovables", external_id: uuidv4()}}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });
    ////////////////Malla Curricular///////////////
    Malla.findOrCreate({where: {anio: '2013'}, defaults: {id: "1", archivo: 'MALLA AJUSTADA 2013-1.pdf', anio: '2013', estado: true, id_carrera: "1", external_id: uuidv4()}}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });
    ///////////ciclo///////////////////////////////
    Ciclo.findOrCreate({where: {numero: '1'}, defaults: {id: "1", numero: '1', nombre: 'Ciencias basicas de la ingenieria', id_mallaCurricular: "1"}}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });
    Ciclo.findOrCreate({where: {numero: '2'}, defaults: {id: "2", numero: '2', nombre: 'Bases sientifico-tecnicas de la formacion profecional en sistemas informaticos', id_mallaCurricular: "1"}}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });
    Ciclo.findOrCreate({where: {numero: '3'}, defaults: {id: "3", numero: '3', nombre: 'Formacion basica del programador', id_mallaCurricular: "1"}}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });
    Ciclo.findOrCreate({where: {numero: '4'}, defaults: {id: "4", numero: '4', nombre: 'Formacion basica de un administrador de base de datos', id_mallaCurricular: "1"}}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });
    Ciclo.findOrCreate({where: {numero: '5'}, defaults: {id: "5", numero: '5', nombre: 'Formacion del programador para la construccion de aplicaciones especificas', id_mallaCurricular: "1"}}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });
    Ciclo.findOrCreate({where: {numero: '6'}, defaults: {id: "6", numero: '6', nombre: 'Formacion del analista de sistemas', id_mallaCurricular: "1"}}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });
    Ciclo.findOrCreate({where: {numero: '7'}, defaults: {id: "7", numero: '7', nombre: 'Desarrollo de sistemas informaticos', id_mallaCurricular: "1"}}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });
    Ciclo.findOrCreate({where: {numero: '8'}, defaults: {id: "8", numero: '8', nombre: 'Gestion de redes y centros de computo', id_mallaCurricular: "1"}}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });
    Ciclo.findOrCreate({where: {numero: '9'}, defaults: {id: "9", numero: '9', nombre: 'Desarrollo de componentes y modelos para software base', id_mallaCurricular: "1"}}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });
    Ciclo.findOrCreate({where: {numero: '10'}, defaults: {id: "10", numero: '10', nombre: 'Sistemas inteligentes y automatizados', id_mallaCurricular: "1"}}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });

    ///////////materia////////////
    ///////////Primer ciclo////////////
    Materia.findOrCreate({where: {nombre: 'Fisica'}, defaults: {
            id: "1",
            nombre: "Fisica",
            creditos: "8",
            codigo: "A-001",
            duracion: "100",
            obligatoria: true,
            id_ciclo: "1",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });
    Materia.findOrCreate({where: {nombre: 'Calculo Diferencial'}, defaults: {
            id: "2",
            nombre: "Calculo Diferencial",
            creditos: "8",
            codigo: "A-001",
            duracion: "100",
            obligatoria: true,
            id_ciclo: "1",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });
    Materia.findOrCreate({where: {nombre: 'Quimica'}, defaults: {
            id: "3",
            nombre: "Quimica",
            creditos: "6",
            codigo: "A-001",
            duracion: "100",
            obligatoria: true,
            id_ciclo: "1",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });
    Materia.findOrCreate({where: {nombre: 'Fundamentos Informaticos'}, defaults: {
            id: "4",
            nombre: "Fundamentos Informaticos",
            creditos: "8",
            codigo: "A-001",
            duracion: "100",
            obligatoria: true,
            id_ciclo: "1",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });
    Materia.findOrCreate({where: {nombre: 'Exprecion Oral y Escrita'}, defaults: {
            id: "5",
            nombre: "Exprecion Oral y Escrita",
            creditos: "4",
            codigo: "B-001",
            id_ciclo: "1",
            duracion: "100",
            obligatoria: true,
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });
    ///////////Segundo ciclo////////////
    Materia.findOrCreate({where: {nombre: 'Fisica II'}, defaults: {
            id: "6",
            nombre: "Fisica II",
            creditos: "6",
            codigo: "A-001",
            duracion: "100",
            obligatoria: true,
            id_ciclo: "2",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });

    Materia.findOrCreate({where: {nombre: 'Calculo Integral'}, defaults: {
            id: "7",
            nombre: "Calculo Integral",
            creditos: "6",
            codigo: "A-001",
            duracion: "100",
            obligatoria: true,
            id_ciclo: "2",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });

    Materia.findOrCreate({where: {nombre: 'Algebra Lineal'}, defaults: {
            id: "8",
            nombre: "Algebra Lineal",
            creditos: "5",
            codigo: "A-001",
            duracion: "100",
            obligatoria: true,
            id_ciclo: "2",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });

    Materia.findOrCreate({where: {nombre: 'Programacion I'}, defaults: {
            id: "9",
            nombre: "Programacion I",
            creditos: "6",
            codigo: "C-001",
            duracion: "100",
            obligatoria: true,
            id_ciclo: "2",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });

    Materia.findOrCreate({where: {nombre: 'Estructura de Datos'}, defaults: {
            id: "10",
            nombre: "Estructura de Datos",
            creditos: "4",
            codigo: "D-001",
            duracion: "100",
            obligatoria: true,
            id_ciclo: "2",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });

    Materia.findOrCreate({where: {nombre: 'Probabilidad e Inferencia Estadistica'}, defaults: {
            id: "11",
            nombre: "Probabilidad e Inferencia Estadistica",
            creditos: "4",
            codigo: "A-001",
            duracion: "100",
            obligatoria: true,
            id_ciclo: "2",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });

    Materia.findOrCreate({where: {nombre: 'Ecologia y Medio Ambiente Tecnologico'}, defaults: {
            id: "12",
            nombre: "Ecologia y Medio Ambiente Tecnologico",
            creditos: "2",
            codigo: "B-001",
            duracion: "100",
            obligatoria: true,
            id_ciclo: "2",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });
    ///////////Tercer ciclo////////////
    Materia.findOrCreate({where: {nombre: 'Electronica Digital'}, defaults: {
            id: "13",
            nombre: "Electronica Digital",
            creditos: "5",
            codigo: "C-001",
            duracion: "100",
            obligatoria: true,
            id_ciclo: "3",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });

    Materia.findOrCreate({where: {nombre: 'Progracion II'}, defaults: {
            id: "14",
            nombre: "Progracion II",
            creditos: "8",
            codigo: "C-001",
            duracion: "100",
            obligatoria: true,
            id_ciclo: "3",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });

    Materia.findOrCreate({where: {nombre: 'Ingieneria del Software I'}, defaults: {
            id: "15",
            nombre: "Ingieneria del Software I",
            creditos: "6",
            codigo: "D-001",
            duracion: "100",
            obligatoria: true,
            id_ciclo: "3",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });

    Materia.findOrCreate({where: {nombre: 'Base de Datos I'}, defaults: {
            id: "16",
            nombre: "Base de Datos I",
            creditos: "6",
            duracion: "100",
            obligatoria: true,
            codigo: "C-001",
            id_ciclo: "3",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });

    Materia.findOrCreate({where: {nombre: 'Estructura de Datos II'}, defaults: {
            id: "17",
            nombre: "Estructura de Datos II",
            creditos: "6",
            duracion: "100",
            obligatoria: true,
            codigo: "D-001",
            id_ciclo: "3",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });

    Materia.findOrCreate({where: {nombre: 'Matematicas Discretas'}, defaults: {
            id: "18",
            nombre: "Matematicas Discretas",
            creditos: "4",
            duracion: "100",
            obligatoria: true,
            codigo: "A-001",
            id_ciclo: "3",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });

    ///////// Cuarto Ciclo //////////

    Materia.findOrCreate({where: {nombre: 'Economia'}, defaults: {
            id: "19",
            nombre: "Economia",
            creditos: "4",
            duracion: "100",
            obligatoria: true,
            codigo: "B-001",
            id_ciclo: "4",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });

    Materia.findOrCreate({where: {nombre: 'Contabilidad General'}, defaults: {
            id: "20",
            nombre: "Contabilidad General",
            creditos: "6",
            duracion: "100",
            obligatoria: true,
            codigo: "B-001",
            id_ciclo: "4",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });

    Materia.findOrCreate({where: {nombre: 'Diseño y Gestion de Base de Datos'}, defaults: {
            id: "21",
            nombre: "Diseño y Gestion de Base de Datos",
            creditos: "10",
            duracion: "100",
            obligatoria: true,
            codigo: "C-001",
            id_ciclo: "4",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });

    Materia.findOrCreate({where: {nombre: 'Metodologia de la Programacion'}, defaults: {
            id: "22",
            nombre: "Metodologia de la Programacion",
            creditos: "10",
            codigo: "A-001",
            duracion: "100",
            obligatoria: true,
            id_ciclo: "4",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });

    Materia.findOrCreate({where: {nombre: 'Metodologia de la Investigacion'}, defaults: {
            id: "23",
            nombre: "Metodologia de la Investigacion",
            creditos: "6",
            codigo: "A-001",
            duracion: "100",
            obligatoria: true,
            id_ciclo: "4",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });

    ///////// Quinto ciclo /////////

    Materia.findOrCreate({where: {nombre: 'Estadistica Inferencial'}, defaults: {
            id: "24",
            nombre: "Estadistica Inferencial",
            creditos: "6",
            duracion: "100",
            obligatoria: true,
            codigo: "A-001",
            id_ciclo: "5",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });

    Materia.findOrCreate({where: {nombre: 'Contabilidad de Costos'}, defaults: {
            id: "25",
            nombre: "Contabilidad de Costos",
            creditos: "5",
            duracion: "100",
            obligatoria: true,
            codigo: "B-001",
            id_ciclo: "5",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });

    Materia.findOrCreate({where: {nombre: 'Arquitectura de Computadoras'}, defaults: {
            id: "26",
            nombre: "Arquitectura de Computadoras",
            creditos: "10",
            codigo: "D-001",
            duracion: "100",
            obligatoria: true,
            id_ciclo: "5",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });
    Materia.findOrCreate({where: {nombre: 'Programacion Avanzada'}, defaults: {
            id: "27",
            nombre: "Programacion Avanzada",
            creditos: "10",
            codigo: "C-001",
            duracion: "100",
            obligatoria: true,
            id_ciclo: "5",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });

    Materia.findOrCreate({where: {nombre: 'Etica Profesional'}, defaults: {
            id: "55",
            nombre: "Programacion Avanzada",
            creditos: "10",
            codigo: "D-001",
            duracion: "100",
            obligatoria: true,
            id_ciclo: "5",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });

    //////////// Sexto ciclo /////////////

    Materia.findOrCreate({where: {nombre: 'Diseño Digital'}, defaults: {
            id: "28",
            nombre: "Diseño Digital",
            creditos: "6",
            codigo: "C-001",
            id_ciclo: "6",
            duracion: "100",
            obligatoria: true,
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });

    Materia.findOrCreate({where: {nombre: 'Administracion de Empresas'}, defaults: {
            id: "29",
            nombre: "Administracion de Empresas",
            creditos: "6",
            codigo: "B-001",
            id_ciclo: "6",
            duracion: "100",
            obligatoria: true,
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });

    Materia.findOrCreate({where: {nombre: 'Analisis y Diseño de Sistemas'}, defaults: {
            id: "30",
            nombre: "Analisis y Diseño de Sistemas",
            creditos: "10",
            codigo: "D-001",
            id_ciclo: "6",
            duracion: "100",
            obligatoria: true,
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });

    Materia.findOrCreate({where: {nombre: 'Lenguaje Ensamblador'}, defaults: {
            id: "31",
            nombre: "Lenguaje Ensamblador",
            creditos: "6",
            codigo: "C-001",
            id_ciclo: "6",
            duracion: "100",
            obligatoria: true,
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });

    Materia.findOrCreate({where: {nombre: 'Simulacion'}, defaults: {
            id: "32",
            nombre: "Simulacion",
            creditos: "8",
            codigo: "D-001",
            duracion: "100",
            obligatoria: true,
            id_ciclo: "6",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });
    //////////////// Septimo ciclo ///////////

    Materia.findOrCreate({where: {nombre: 'Ecuaciones Diferenciales'}, defaults: {
            id: "33",
            nombre: "Ecuaciones Diferenciales",
            creditos: "5",
            codigo: "D-001",
            duracion: "100",
            obligatoria: true,
            id_ciclo: "7",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });

    Materia.findOrCreate({where: {nombre: 'Sistemas Operativos'}, defaults: {
            id: "34",
            nombre: "Sistemas Operativos",
            creditos: "8",
            codigo: "D-001",
            duracion: "100",
            obligatoria: true,
            id_ciclo: "7",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });

    Materia.findOrCreate({where: {nombre: 'Diseño de Sistemas'}, defaults: {
            id: "35",
            nombre: "Diseño de Sistemas",
            creditos: "8",
            codigo: "D-001",
            duracion: "100",
            obligatoria: true,
            id_ciclo: "7",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });

    Materia.findOrCreate({where: {nombre: 'Teoria de Telecomunicaciones'}, defaults: {
            id: "36",
            nombre: "Teoria de Telecomunicaciones",
            creditos: "6",
            codigo: "D-001",
            duracion: "100",
            obligatoria: true,
            id_ciclo: "7",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });

    Materia.findOrCreate({where: {nombre: 'Derecho Informatico'}, defaults: {
            id: "37",
            nombre: "Derecho Informatico",
            creditos: "4",
            codigo: "D-001",
            duracion: "100",
            obligatoria: true,
            id_ciclo: "7",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });

    Materia.findOrCreate({where: {nombre: 'Proyectos Informaticos I'}, defaults: {
            id: "38",
            nombre: "Proyectos Informaticos I",
            creditos: "5",
            codigo: "D-001",
            duracion: "100",
            obligatoria: true,
            id_ciclo: "7",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });

    ////////// Octavo Ciclo ////////////

    Materia.findOrCreate({where: {nombre: 'Investigacion de Operaciones'}, defaults: {
            id: "39",
            nombre: "Investigacion de Operaciones",
            creditos: "6",
            duracion: "100",
            obligatoria: true,
            codigo: "D-001",
            id_ciclo: "8",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });

    Materia.findOrCreate({where: {nombre: 'Analisis Numerico'}, defaults: {
            id: "40",
            nombre: "Analisis Numerico",
            creditos: "6",
            duracion: "100",
            obligatoria: true,
            codigo: "D-001",
            id_ciclo: "8",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });

    Materia.findOrCreate({where: {nombre: 'Administracion de Centros de Computo'}, defaults: {
            id: "41",
            nombre: "Administracion de Centros de Computo",
            creditos: "6",
            duracion: "100",
            obligatoria: true,
            codigo: "D-001",
            id_ciclo: "8",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });

    Materia.findOrCreate({where: {nombre: 'Gestion de Redes'}, defaults: {
            id: "42",
            nombre: "Gestion de Redes",
            creditos: "8",
            duracion: "100",
            obligatoria: true,
            codigo: "C-001",
            id_ciclo: "8",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });

    Materia.findOrCreate({where: {nombre: 'Auditoria Informatica'}, defaults: {
            id: "43",
            nombre: "Auditoria Informatica",
            creditos: "8",
            codigo: "D-001",
            duracion: "100",
            obligatoria: true,
            id_ciclo: "8    ",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });


    Materia.findOrCreate({where: {nombre: 'Proyectos Informaticos II'}, defaults: {
            id: "44",
            nombre: "Proyectos Informaticos II",
            creditos: "4",
            duracion: "100",
            obligatoria: true,
            codigo: "D-001",
            id_ciclo: "8",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });



    //////////////////////// Noveno Ciclo //////////////////////

    Materia.findOrCreate({where: {nombre: 'Modelamiento Matematico'}, defaults: {
            id: "45",
            nombre: "Modelamiento Matematico",
            creditos: "4",
            codigo: "C-001",
            duracion: "100",
            obligatoria: true,
            id_ciclo: "9",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });

    Materia.findOrCreate({where: {nombre: 'Automatas y Lenguajes Formales'}, defaults: {
            id: "46",
            nombre: "Automatas y Lenguajes Formales",
            creditos: "4",
            codigo: "D-001",
            duracion: "100",
            obligatoria: true,
            id_ciclo: "9",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });
    Materia.findOrCreate({where: {nombre: 'Inteligencia Artificial'}, defaults: {
            id: "47",
            nombre: "Inteligencia Artificial",
            creditos: "10",
            codigo: "D-001",
            id_ciclo: "9",
            duracion: "100",
            obligatoria: true,
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });
    Materia.findOrCreate({where: {nombre: 'Sistemas de Informacion'}, defaults: {
            id: "48",
            nombre: "Sistemas de Informacion",
            creditos: "7",
            codigo: "D-001",
            duracion: "100",
            obligatoria: true,
            id_ciclo: "9",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });
    Materia.findOrCreate({where: {nombre: 'Proyecto de Trabajo de Titulacion'}, defaults: {
            id: "49",
            nombre: "Proyecto de Trabajo de Titulacion",
            creditos: "5",
            codigo: "D-001",
            duracion: "100",
            obligatoria: true,
            id_ciclo: "9",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });

    Materia.findOrCreate({where: {nombre: 'Ingieneria del Software II'}, defaults: {
            id: "50",
            nombre: "Ingieneria del Software II",
            creditos: "6",
            codigo: "D-001",
            duracion: "100",
            obligatoria: true,
            id_ciclo: "9",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });
/////////////////// Decimo Ciclo //////////////////

    Materia.findOrCreate({where: {nombre: 'Control Automatizado Asistido por Comnputadores'}, defaults: {
            id: "51",
            nombre: "Control Automatizado Asistido por Comnputadores",
            creditos: "4",
            duracion: "100",
            obligatoria: true,
            codigo: "C-001",
            id_ciclo: "10",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });

    Materia.findOrCreate({where: {nombre: 'Compiladores'}, defaults: {
            id: "52",
            nombre: "Compiladores",
            creditos: "4",
            codigo: "D-001",
            duracion: "100",
            obligatoria: true,
            id_ciclo: "10",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });

    Materia.findOrCreate({where: {nombre: 'Sistemas  Expertos'}, defaults: {
            id: "53",
            nombre: "Sistemas  Expertos",
            creditos: "4",
            codigo: "D-001",
            duracion: "100",
            obligatoria: true,
            id_ciclo: "10",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });
    Materia.findOrCreate({where: {nombre: 'Trabajos de Titulacion'}, defaults: {
            id: "54",
            nombre: "Trabajos de Titulacion",
            creditos: "20",
            codigo: "D-001",
            duracion: "100",
            obligatoria: true,
            id_ciclo: "10",
            external_id: uuidv4(),
        }}).spread((roll, created) => {
        console.log(roll.get({
            plain: true
        }));
    });

};

module.exports = init();