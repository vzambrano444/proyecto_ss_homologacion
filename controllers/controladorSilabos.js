'use strict';
var models = require('../models/');
const uuidv4 = require('uuid/v4');
var Periodo = models.periodoAcademico;
var Silabo = models.silabo;
var Materia = models.materia;
var Docente = models.docente;
var Persona = models.persona;
var root = require('app-root-path');
//para subir imagen
var fs = require('fs');
var maxFileSize = 2 * 1024 * 1024;
var extensiones = ["pdf"];
var formidable = require('formidable');
/**
 * Clase que permite manipular los datos del modelo periodo, silabo, materia, docente, persona
 */
class controladorSilabos {
    /**
     * Funcion que permite modificar mostrar la vista de los silabos cargados
     * @param {type} req el objeto peticion
     * @param {type} res el objeto respuesta
     * @returns {undefined} redirecciona a la pagina /lista/silabos
     */
    listarSilabos(req, res) {
        if (req.user.rolN === 'Decano') {
            Silabo.findAll({include: [{model: Docente, include: {model: Persona}}, {model: Periodo}, {model: Materia}]}).then(function (silabo) {
                if (silabo) {
                    res.render('partials/decano/index',
                            {titulo: "Silabos",
                                partial: 'decano/frm_silabos',
                                Silabo: silabo,
                                nombre: req.user.nom,
                                rol: req.user.rolN,
                                foto: req.user.foto,
                                idPersona: req.user.idPersona,
                                msg: {
                                    error: req.flash('error'),
                                    info: req.flash("info")
                                }
                            });
                }
            }).catch(function (err) {
                req.flash('info', 'Hubo un error');
                res.redirect('/');
            });
        } else if (req.user.rolN === 'Docente') {

            Docente.findOne({where: {id_persona: req.user.idPersona}}).then(function (docente) {
                if (docente) {
                    Materia.findAll({}).then(function (materia) {
                        if (materia) {
                            Periodo.findAll({where: {estado: true}}).then(function (periodo) {
                                if (periodo) {
                                    Silabo.findAll({where: {id_docente: docente.id}, include: [{model: Docente}, {model: Periodo}, {model: Materia}]}).then(function (silabo) {
                                        console.log(silabo);
                                        if (silabo) {
                                            // res.send(silabo);
                                            res.render('partials/docente/index',
                                                    {titulo: "Silabos",
                                                        partial: 'docente/frm_silabos',
                                                        Silabo: silabo,
                                                        Materia: materia,
                                                        Periodo: periodo,
                                                        nombre: req.user.nom,
                                                        rol: req.user.rolN,
                                                        foto: req.user.foto,
                                                        msg: {
                                                            error: req.flash('error'),
                                                            info: req.flash("info")
                                                        }
                                                    });
                                        }
                                    }).catch(function (err) {
                                        req.flash('error', 'Hubo un error');
                                        res.redirect('/');
                                    });
                                }
                            });
                        }
                    });
                }
            });
        } else if (req.user.rolN === 'Abogado') {
            res.redirect('/perfil')
        } else if (req.user.rolN === 'Solicitante') {
            res.redirect('/perfil')
        }
    }
    /**
     * Funcion que permite cargar los silabos por parte del docente
     * @param {type} req el objeto peticion
     * @param {type} res el objeto respuesta
     * @returns {undefined} redirecciona a la pagina /lista/silabos
     */
    guardarSilabos(req, res) {

        Docente.findOne({where: {id_persona: req.user.idPersona}}).then(function (docente) {
            if (docente) {
                var form = new formidable.IncomingForm();
                form.parse(req, function (err, fields, files) {

                    Silabo.findOne({where: {id_periodoAcademico: fields.periodo}}).then(function (silabo) {
                        if (silabo) {
                            req.flash('info', 'El silabo ya existe');
                            res.redirect('/lista/silabos');
                        } else {
                            Silabo.create({
                                archivo: 'sin_archivo.pdf',
                                id_docente: docente.id,
                                id_periodoAcademico: fields.periodo,
                                id_materia: fields.materia,
                                estado: true,
                                external_id: uuidv4()
                            }).then(function (newSilabo, created) {
                                if (newSilabo) {
                                    if (files.archivo.size <= maxFileSize) {
                                        console.log('****************' + root);
                                        var extension = files.archivo.name.split(".").pop().toLowerCase();
                                        if (extension != "pdf") {
                                            req.flash('error', 'Solo se admiten archivos .pdf');
                                            res.redirect('/lista/silabos');
                                        } else {
                                            if (extensiones.includes(extension)) {
                                                var oldpath = files.archivo.path;
                                                console.log('por que no entra');
                                                var nombre = newSilabo.external_id + "." + extension;
                                                console.log(files.archivo.path);
                                                fs.readFile(oldpath, function (err, data) {
                                                    // Write the file
                                                    fs.writeFile((root + "/public/upload/silabos/" + nombre), data, function (error) {
                                                        if (error)
                                                            throw err;
                                                        console.log('File written!');
                                                        Silabo.update({
                                                            archivo: nombre
                                                        }, {where: {external_id: newSilabo.external_id}}).then(function (updatedPersona, created) {
                                                            if (updatedPersona) {
                                                                req.flash('info', 'se subio correctamente');
                                                                res.redirect('/lista/silabos');
                                                            }

                                                        });
                                                    });
                                                    // Delete the file
                                                    fs.unlink(oldpath, function (err) {
                                                        if (err)
                                                            throw err;
                                                        console.log('File deleted!');
                                                    });
                                                });
                                            } else {
                                                controladorSilabos.eliminar(files.archivo.path);
                                                console.log('Error de extencion');
                                                req.flash('error', 'Error de extencion', false);
                                                res.redirect('/lista/silabos' + fields.external);
                                            }
                                        }
                                    } else {
                                        controladorSilabos.eliminar(files.archivo.path);
                                        console.log('Error de tamaño se admite');
                                        req.flash('error', 'Error de tamaño se admite', +maxFileSize, false);
                                        res.redirect('/lista/silabos' + fields.external);
                                    }
                                }
                            }).catch(function (err) {
                                console.log(err);
                                req.flash('error', 'Hubo un error');
                                res.redirect('/');
                            });
                        }
                    }).catch(function (err) {
                        req.flash('info', 'Hubo un error');
                        res.redirect('/');
                    });
                });
            }
        });


    }
    /**
     * Funcion que permite buscar un silabo para mostrarlo en ul 
     * @param {type} req el objeto peticion
     * @param {type} res el objeto respuesta
     * @returns {undefined} retorna un json con el la informacion del silavo
     */
    silabosAPI(req, res) {
        var texto = req.params.texto;
        Silabo.findOne({where: {external_id: texto}, include: [{model: Docente}, {model: Periodo, where: {estado: true}}, {model: Materia}]}).then(function (silabo) {
            res.status(200).json(silabo);
        });
    }
    /**
     * Funcion que permite editar los archivos de los silabos 
     * @param {type} req el objeto peticion
     * @param {type} res el objeto respuesta
     * @returns {undefined} /lista/silabos
     */
    editarSilabos(req, res) {
        var form = new formidable.IncomingForm();
        form.parse(req, function (err, fields, files) {
            console.log('+++++++++++++++++');
            if (files.archivo.size <= maxFileSize) {
                console.log('****************' + root);
                var extension = files.archivo.name.split(".").pop().toLowerCase();
                if (extensiones.includes(extension)) {
                    var oldpath = files.archivo.path;
                    console.log('por que no entra');
                    var nombre = fields.external + "." + extension;
                    console.log(files.archivo.path);
                    fs.readFile(oldpath, function (err, data) {
                        // Write the file
                        fs.writeFile((root + "/public/upload/silabos/" + nombre), data, function (error) {
                            if (error)
                                throw err;
                            console.log('File written!');
                            Silabo.update({
                                archivo: nombre
                            }, {where: {external_id: fields.external}}).then(function (updatedPersona, created) {
                                if (updatedPersona) {
                                    req.flash('info', 'se subio correctamente', false);
                                    res.redirect('/lista/silabos');
                                }
                            });
                        });
                        // Delete the file
                        fs.unlink(oldpath, function (err) {
                            if (err)
                                throw err;
                            console.log('File deleted!');
                        });
                    });
                } else {
                    controladorSilabos.eliminar(files.archivo.path);
                    console.log('Error de extencion');
                    req.flash('error', 'Error de extencion', false);
                    res.redirect('/lista/silabos' + fields.external);
                }
            } else {
                controladorSilabos.eliminar(files.archivo.path);
                console.log('Error de tamaño se admite');
                req.flash('error', 'Error de tamaño se admite', +maxFileSize, false);
                res.redirect('/lista/silabos' + fields.external);
            }
        });
    }
    static eliminar(link) {
        fs.exists(link, function (exists) {
            if (exists) {
                console.log('files exists, deleting now ....' + link);
                fs.unlinkSync(link);
            } else {
                console.log('no se borro' + link);
            }
        });
    }
    /**
     * Funcion que permite editar los estados de los silabos 
     * @param {type} req el objeto peticion
     * @param {type} res el objeto respuesta
     * @returns {undefined} /lista/silabos
     */
    bloquearSilabo(req, res) {
        Silabo.update({
            estado: req.body.estado
        }, {where: {external_id: req.body.external}}).then(function (updatedPersona, created) {
            if (updatedPersona) {
                req.flash('info', 'Accion exitosa');
                res.redirect('/lista/silabos');
            }
        });
    }

}

module.exports = controladorSilabos;


