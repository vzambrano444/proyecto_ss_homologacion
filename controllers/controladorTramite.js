'use strict';
var models = require('../models/');
const uuidv4 = require('uuid/v4');
const Sequelize = require('sequelize');
/**
 * sincronizacion a base de datos
 */
const sequelize = new Sequelize('SistemaHomologacion', 'root', '12345', {
    host: 'localhost',
    dialect: 'mysql',
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
    storage: 'path/to/database.sqlite',
    operatorsAliases: false
});

var root = require('app-root-path');
//para subir imagen
var fs = require('fs');
var maxFileSize = 20 * 1024 * 1024;
var extensiones = ["zip", "rar"];
var formidable = require('formidable');
var Persona = models.persona;
var Docente = models.docente;
var Carrera = models.carrera;
var Archivo = models.archivo;
var Tramite = models.tramite;
var Seguimiento = models.seguimiento;
/**
 * Clase que permite manipular los datos del modelo persona, docente, carrera, archivo, tramite, seguimiento
 */
class controladorTramite {
    /**
     * Funcion que permite mostrar la vista para visualizar los tramites de homologacion
     * @param {type} req el objeto peticion
     * @param {type} res el objeto respuesta
     * @returns {undefined} redirecciona a la pagina /menu/tramite
     */
    visualizarTramite(req, res) {
        if (req.user.rolN === "Decano") {
            var Lista = {};
            Carrera.findAll({}).then(function (carrera) {
                if (carrera) {
                    Docente.findAll({include: {model: Persona}}).then(function (docente) {
                        if (docente) {

                            Tramite.findAll({include: [{model: Persona}, {model: Archivo}, {model: Seguimiento, as: "seguimiento", where: {revDocumentacion: "1"}}]}).then(function (tramite) {
                                if (tramite) {

                                    tramite.forEach(function (ltramite, i) {
                                        console.log("****************************yupi1**********************************");
                                        carrera.forEach(function (lcarrera, j) {
                                            console.log("****************************yupi2**********************************");
                                            docente.forEach(function (ldocente, k) {
                                                if (ltramite.external_docente === ldocente.external_id && ltramite.external_carrera === lcarrera.external_id) {
                                                    console.log("****************************yupi3**********************************");
                                                    //  console.log("****"+ldocente.persona.nombres[k] + "////////*****************/////////// " + ldocente.persona.apellidos[k]);
                                                    Lista[i] = {
                                                        registro: ltramite.registro,
                                                        solicitante: ltramite.persona.nombres + " " + ltramite.persona.apellidos,
                                                        estado: ltramite.estado,
                                                        external_docente: ltramite.external_docente,
                                                        external_carrera: ltramite.external_carrera,
                                                        tipo: ltramite.tipo,
                                                        external_id: ltramite.external_id,
                                                        docente: ldocente.persona.nombres + " " + ldocente.persona.apellidos,
                                                        fotD: ldocente.persona.foto,
                                                        carrera: lcarrera.nombre,
                                                        archivo: ltramite.archivo.archivo,
                                                        informe: ltramite.archivo.informe_peticionario,
                                                        foto: ltramite.persona.foto
                                                    };
                                                }
                                            });
                                        });
                                    });

                                    res.render('partials/decano/index',
                                            {titulo: "Tramites",
                                                partial: 'decano/frm_tramite',
                                                nombre: req.user.nom,
                                                rol: req.user.rolN,
                                                foto: req.user.foto,

                                                Carrera: carrera,
                                                Tramite: Lista,
                                                idPersona: req.user.idPersona,
                                                msg: {
                                                    error: req.flash('error'),
                                                    info: req.flash("info")
                                                }
                                            });
                                }
                            }).catch(function (err) {
                                req.flash('error', 'Hubo un error' + err);
                                res.redirect('/');
                            });
                        }
                    }).catch(function (err) {
                        req.flash('error', 'Hubo un error' + err);
                        res.redirect('/');
                    });
                }
            }).catch(function (err) {
                req.flash('info', 'Hubo un error');
                res.redirect('/');
            });
        } else if (req.user.rolN === "Abogado") {
            var Lista = {};
            Carrera.findAll({}).then(function (carrera) {
                if (carrera) {
                    Docente.findAll({include: {model: Persona}}).then(function (docente) {
                        if (docente) {
                            // res.send(docente);
                            Seguimiento.findAll({where: {revSolicitud: "1"}, include: {model: Tramite, include: [{model: Persona}, {model: Archivo}]}}).then(function (tramite) {
                                if (tramite) {

                                    tramite.forEach(function (ltramite, i) {
                                        //res.send(ltramite);
                                        carrera.forEach(function (lcarrera, j) {
                                            docente.forEach(function (ldocente, k) {
                                                if (ltramite.tramite.external_docente === ldocente.external_id && ltramite.tramite.external_carrera === lcarrera.external_id) {
                                                    Lista[i] = {
                                                        registro: ltramite.tramite.registro,
                                                        solicitante: ltramite.tramite.persona.nombres + " " + ltramite.tramite.persona.apellidos,
                                                        estado: ltramite.revSellos,
                                                        external_docente: ltramite.tramite.external_docente,
                                                        external_carrera: ltramite.tramite.external_carrera,
                                                        tipo: ltramite.tramite.tipo,
                                                        external_id: ltramite.external_id,
                                                        docente: ldocente.persona.nombres + " " + ldocente.persona.apellidos,
                                                        carrera: lcarrera.nombre,
                                                        archivo: ltramite.tramite.archivo.archivo,
                                                        fotoD: ldocente.persona.foto,
                                                        foto: ltramite.tramite.persona.foto,
                                                    };

                                                }
                                            });
                                        });
                                    });
                                    //res.send(Lista);
                                    res.render('partials/abogado/index',
                                            {titulo: "Tramites",
                                                partial: 'abogado/frm_tramite',
                                                nombre: req.user.nom,
                                                rol: req.user.rolN,
                                                foto: req.user.foto,
                                                Tramite: Lista,
                                                idPersona: req.user.idPersona,
                                                msg: {
                                                    error: req.flash('error'),
                                                    info: req.flash("info")
                                                }
                                            });
                                }
                            }).catch(function (err) {
                                req.flash('error', 'Hubo un error' + err);
                                res.redirect('/');
                            });
                        }
                    }).catch(function (err) {
                        req.flash('error', 'Hubo un error' + err);
                        res.redirect('/');
                    });
                }
            }).catch(function (err) {
                req.flash('info', 'Hubo un error');
                res.redirect('/');
            });
        } else if (req.user.rolN === "Docente") {
            var Lista = {};
            Docente.findOne({where: {id_persona: req.user.idPersona}, include: {model: Persona}}).then(function (asignado) {

                Carrera.findAll({}).then(function (carrera) {
                    if (carrera) {
                        Docente.findAll({include: {model: Persona}}).then(function (docente) {
                            if (docente) {
                                // 
                                Seguimiento.findAll({where: {asigDocente: "1"}, include: {model: Tramite, where: {external_docente: asignado.external_id}, include: [{model: Persona}, {model: Archivo}]}}).then(function (tramite) {
                                    if (tramite) {

                                        tramite.forEach(function (ltramite, i) {
                                            //res.send(ltramite);
                                            carrera.forEach(function (lcarrera, j) {
                                                docente.forEach(function (ldocente, k) {
                                                    if (ltramite.tramite.external_docente === ldocente.external_id && ltramite.tramite.external_carrera === lcarrera.external_id) {
                                                        Lista[i] = {
                                                            registro: ltramite.tramite.registro,
                                                            solicitante: ltramite.tramite.persona.nombres + " " + ltramite.tramite.persona.apellidos,
                                                            estado: ltramite.revDocumentacion,
                                                            external_docente: ltramite.tramite.external_docente,
                                                            external_carrera: ltramite.tramite.external_carrera,
                                                            tipo: ltramite.tramite.tipo,
                                                            external_idS: ltramite.external_id,
                                                            external_idA: ltramite.tramite.archivo.external_id,
                                                            docente: ldocente.persona.nombres + " " + ldocente.persona.apellidos,
                                                            carrera: lcarrera.nombre,
                                                            archivo: ltramite.tramite.archivo.archivo,
                                                            informe: ltramite.tramite.archivo.informe_peticionario,
                                                            external_docente: ldocente.external_id,
                                                            fotoD: ldocente.persona.foto,
                                                            foto: ltramite.tramite.persona.foto,
                                                        };

                                                    }
                                                });
                                            });
                                        });
                                        //res.send(Lista);
                                        res.render('partials/docente/index',
                                                {titulo: "Tramites",
                                                    partial: 'docente/frm_tramite',
                                                    nombre: req.user.nom,
                                                    rol: req.user.rolN,
                                                    foto: req.user.foto,
                                                    Tramite: Lista,
                                                    idPersona: req.user.idPersona,
                                                    msg: {
                                                        error: req.flash('error'),
                                                        info: req.flash("info")
                                                    }
                                                });
                                    }
                                }).catch(function (err) {
                                    req.flash('error', 'Hubo un error' + err);
                                    res.redirect('/');
                                });
                            }
                        }).catch(function (err) {
                            req.flash('error', 'Hubo un error' + err);
                            res.redirect('/');
                        });
                    }
                }).catch(function (err) {
                    req.flash('info', 'Hubo un error');
                    res.redirect('/');
                });
            }).catch(function (err) {
                req.flash('info', 'Hubo un error');
                res.redirect('/');
            });
        } else if (req.user.rolN === "Solicitante") {
            Carrera.findAll({}).then(function (carrera) {
                if (carrera) {
                    Docente.findAll({}).then(function (docente) {
                        if (docente) {
                            sequelize.query("select * from tramite,archivo,carrera  where tramite.id_persona = " + req.user.idPersona + " AND tramite.id_archivo = archivo.id AND tramite.external_carrera = carrera.external_id",
                                    {type: sequelize.QueryTypes.SELECT}).then(function (tramite) {
                                if (tramite) {
                                    // res.send(tramite);
                                    var codigo = "TME-" + (tramite.length + 1);
                                    res.render('partials/solicitante/index',
                                            {titulo: "Tramites",
                                                partial: 'solicitante/frm_tramite',
                                                nombre: req.user.nom,
                                                rol: req.user.rolN,
                                                foto: req.user.foto,
                                                Carrera: carrera,
                                                Codigo: codigo,
                                                Tramite: tramite,
                                                idPersona: req.user.idPersona,
                                                msg: {
                                                    error: req.flash('error'),
                                                    info: req.flash("info")
                                                }
                                            });
                                }
                            }).catch(function (err) {
                                req.flash('error', 'Hubo un error' + err);
                                res.redirect('/');
                            });
                        }
                    }).catch(function (err) {
                        req.flash('error', 'Hubo un error' + err);
                        res.redirect('/');
                    });
                }

            }).catch(function (err) {
                req.flash('info', 'Hubo un error');
                res.redirect('/');
            });
        }
    }
    /**
     * Funcion que permite crear un tramite para el proceso de homologacion
     * @param {type} req el objeto peticion
     * @param {type} res el objeto respuesta
     * @returns {undefined} redirecciona a la pagina /menu/tramite
     */
    guardarTramite(req, res) {
        var form = new formidable.IncomingForm();
        form.parse(req, function (err, fields, files) {
            if (fields.external === "0") {
                Archivo.create({
                    archivo: 'sin_archivo.zip',
                    external_id: uuidv4()
                }).then(function (newArchivo, created) {
                    if (newArchivo) {
                        if (files.archivo.size <= maxFileSize) {
                            console.log('****************' + root);
                            var extension = files.archivo.name.split(".").pop().toLowerCase();
                            if (extension != "zip" && extension != "rar") {
                                req.flash('error', 'Solo se admiten archivos .zip o .rar');
                                res.redirect('/menu/tramite');
                            } else {
                                if (extensiones.includes(extension)) {
                                    var oldpath = files.archivo.path;
                                    console.log('por que no entra');
                                    var nombre = newArchivo.external_id + "." + extension;
                                    console.log(files.archivo.path);
                                    fs.readFile(oldpath, function (err, data) {
                                        // Write the file
                                        fs.writeFile((root + "/public/upload/archivos/" + nombre), data, function (error) {
                                            if (error)
                                                throw err;
                                            console.log('File written!');
                                            Archivo.update({
                                                archivo: nombre
                                            }, {where: {external_id: newArchivo.external_id}}).then(function (updatedArchivo, created) {
                                                if (updatedArchivo) {
                                                    Tramite.create({
                                                        estado: false,
                                                        external_docente: "0",
                                                        external_carrera: fields.carrera,
                                                        tipo: fields.tipo,
                                                        id_persona: req.user.idPersona,
                                                        id_archivo: newArchivo.id,
                                                        registro: fields.registro,
                                                        external_id: uuidv4()
                                                    }).then(function (newTramite, created) {
                                                        if (newTramite) {
                                                            Seguimiento.create({
                                                                revSolicitud: false,
                                                                revSellos: false,
                                                                asigDocente: false,
                                                                revDocumentacion: false,
                                                                id_tramite: newTramite.id,
                                                                external_id: uuidv4()
                                                            }).then(function (newSeguimiento, created) {
                                                                if (newSeguimiento) {
                                                                    req.flash('info', 'Se ha registrado su tramite con exito');
                                                                    res.redirect('/menu/tramite');
                                                                }
                                                            }).catch(function (err) {
                                                                req.flash('info', 'Hubo un error');
                                                                res.redirect('/menu/tramite');
                                                            });
                                                        }
                                                    }).catch(function (err) {
                                                        req.flash('info', 'Hubo un error');
                                                        res.redirect('/menu/tramite');
                                                    });
                                                }

                                            });
                                        });
                                        // Delete the file
                                        fs.unlink(oldpath, function (err) {
                                            if (err)
                                                throw err;
                                            console.log('File deleted!');
                                        });
                                    });
                                } else {
                                    controladorTramite.eliminar(files.archivo.path);
                                    console.log('Error de extencion');
                                    req.flash('error', 'Error de extencion', false);
                                    res.redirect('/menu/tramite' + fields.external);
                                }
                            }
                        } else {
                            controladorTramite.eliminar(files.archivo.path);
                            console.log('Error de tamaño se admite');
                            req.flash('error', 'Error de tamaño se admite', +maxFileSize, false);
                            res.redirect('/menu/tramite' + fields.external);
                        }

                    }
                }).catch(function (err) {
                    console.log(err);
                    req.flash('error', 'Hubo un error');
                    res.redirect('/menu/tramite');
                });
            } else {
                if (files.archivo.size <= maxFileSize) {
                    console.log('****************' + root);
                    var extension = files.archivo.name.split(".").pop().toLowerCase();
                    if (extension != "zip" && extension != "rar") {
                        req.flash('error', 'Solo se admiten archivos .zip o .rar');
                        res.redirect('/menu/tramite');
                    } else {
                        if (extensiones.includes(extension)) {
                            var oldpath = files.archivo.path;
                            console.log('por que no entra');
                            var nombre = fields.external + "." + extension;
                            console.log(files.archivo.path);
                            fs.readFile(oldpath, function (err, data) {
                                // Write the file
                                fs.writeFile((root + "/public/upload/archivos/" + nombre), data, function (error) {
                                    if (error)
                                        throw err;
                                    console.log('File written!');
                                    Archivo.update({
                                        archivo: nombre
                                    }, {where: {external_id: fields.external}}).then(function (updatedArchivo, created) {
                                        if (updatedArchivo) {
                                            Tramite.update({
                                                external_carrera: fields.carrera,
                                                tipo: fields.tipo,
                                            }, {where: {external_id: fields.externalT}}).then(function (newTramite, created) {
                                                if (newTramite) {
                                                    req.flash('info', 'Se ha modificado su tramite con exito');
                                                    res.redirect('/menu/tramite');
                                                }
                                            }).catch(function (err) {
                                                req.flash('info', 'Hubo un error');
                                                res.redirect('/menu/tramite');
                                            });
                                        }

                                    });
                                });
                                // Delete the file
                                fs.unlink(oldpath, function (err) {
                                    if (err)
                                        throw err;
                                    console.log('File deleted!');
                                });
                            });
                        } else {
                            controladorTramite.eliminar(files.archivo.path);
                            console.log('Error de extencion');
                            req.flash('error', 'Error de extencion', false);
                            res.redirect('/menu/tramite' + fields.external);
                        }
                    }
                } else {
                    controladorTramite.eliminar(files.archivo.path);
                    console.log('Error de tamaño se admite');
                    req.flash('error', 'Error de tamaño se admite', +maxFileSize, false);
                    res.redirect('/menu/tramite' + fields.external);
                }
            }
        });
    }
    static eliminar(link) {
        fs.exists(link, function (exists) {
            if (exists) {
                console.log('files exists, deleting now ....' + link);
                fs.unlinkSync(link);
            } else {
                console.log('no se borro' + link);
            }
        });
    }
    /**
     * Funcion que permite desiganr un estado al tramite para saber si esta aceptado o sigue en espera
     * @param {type} req el objeto peticion
     * @param {type} res el objeto respuesta
     * @returns {undefined} redirecciona a la pagina /menu/tramite
     */
    bloquearTramite(req, res) {
        Tramite.update({
            estado: req.body.estado
        }, {where: {external_id: req.body.external}}).then(function (tramite) {
            if (tramite) {
                req.flash('info', 'Se ha registrado correctamente');
                res.redirect('/menu/tramite');
            }
        });
    }

}

module.exports = controladorTramite;

