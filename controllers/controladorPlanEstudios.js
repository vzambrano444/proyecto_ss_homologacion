'use strict';
var models = require('../models/');
var uuid = require('uuid');
var Malla = models.mallaCurricular;
var Ciclo = models.ciclo;
var Carrera = models.carrera;
var Materia = models.materia;
var Silabo = models.silabo;
/**
 * Clase que permite manipular los datos del modelo malla, ciclo, carrera, materia, silabo
 */
class controladorPlanEstudios {
    /**
     * Funcion que permite enlistar un plan de estudios de la carrera para mostrar como guia 
     * @param {type} req el objeto peticion
     * @param {type} res el objeto respuesta
     * @returns {undefined} redirecciona a la pagina /menu/plan
     */
    listarPlan(req, res) {
        if (req.user.rolN === "Decano") {
            Carrera.findOne({where: {id: "1"}}).then(function (carrera) {
                Malla.findOne({where: {id: "1"}}).then(function (malla) {

                    Materia.findAll({include: [{model: Silabo, as: "silabo", where: {estado: '1'}}, {model: Ciclo, where: {id: "1"}}]}).then(function (ciclo1) {
                        if (ciclo1) {
                            Materia.findAll({include: [{model: Silabo, as: "silabo" , where: {estado: '1'}}, {model: Ciclo, where: {id: "2"}}]}).then(function (ciclo2) {
                                if (ciclo2) {
                                    Materia.findAll({include: [{model: Silabo, as: "silabo" , where: {estado: '1'}}, {model: Ciclo, where: {id: "3"}}]}).then(function (ciclo3) {
                                        if (ciclo3) {
                                            Materia.findAll({include: [{model: Silabo, as: "silabo" , where: {estado: '1'}}, {model: Ciclo, where: {id: "4"}}]}).then(function (ciclo4) {
                                                if (ciclo4) {
                                                    Materia.findAll({include: [{model: Silabo, as: "silabo" , where: {estado: '1'}}, {model: Ciclo, where: {id: "5"}}]}).then(function (ciclo5) {
                                                        if (ciclo5) {
                                                            Materia.findAll({include: [{model: Silabo, as: "silabo" , where: {estado: '1'}}, {model: Ciclo, where: {id: "6"}}]}).then(function (ciclo6) {
                                                                if (ciclo6) {
                                                                    Materia.findAll({include: [{model: Silabo, as: "silabo" , where: {estado: '1'}}, {model: Ciclo, where: {id: "7"}}]}).then(function (ciclo7) {
                                                                        if (ciclo7) {
                                                                            Materia.findAll({include: [{model: Silabo, as: "silabo", where: {estado: '1'}}, {model: Ciclo, where: {id: "8"}}]}).then(function (ciclo8) {
                                                                                if (ciclo8) {
                                                                                    Materia.findAll({include: [{model: Silabo, as: "silabo"}, {model: Ciclo, where: {id: "9"}}]}).then(function (ciclo9) {
                                                                                        if (ciclo9) {
                                                                                            Materia.findAll({include: [{model: Silabo, as: "silabo" , where: {estado: '1'}}, {model: Ciclo, where: {id: "10"}}]}).then(function (ciclo10) {
                                                                                                if (ciclo10) {
                                                                                                    res.render('partials/decano/index',
                                                                                                            {titulo: "Plan de Estudios",
                                                                                                                partial: 'decano/frm_plan',
                                                                                                                Carrera: carrera,
                                                                                                                Malla: malla,
                                                                                                                Ciclo1: ciclo1,
                                                                                                                Nombre1: ciclo1[0].ciclo.nombre,
                                                                                                                Ciclo2: ciclo2,
                                                                                                                Nombre2: ciclo2[0].ciclo.nombre,
                                                                                                                Ciclo3: ciclo3,
                                                                                                                Nombre3: ciclo3[0].ciclo.nombre,
                                                                                                                Ciclo4: ciclo4,
                                                                                                                Nombre4: ciclo4[0].ciclo.nombre,
                                                                                                                Ciclo5: ciclo5,
                                                                                                                Nombre5: ciclo5[0].ciclo.nombre,
                                                                                                                Ciclo6: ciclo6,
                                                                                                                Nombre6: ciclo6[0].ciclo.nombre,
                                                                                                                Ciclo7: ciclo7,
                                                                                                                Nombre7: ciclo7[0].ciclo.nombre,
                                                                                                                Ciclo8: ciclo8,
                                                                                                                Nombre8: ciclo8[0].ciclo.nombre,
                                                                                                                Ciclo9: ciclo9,
                                                                                                                Nombre9: ciclo9[0].ciclo.nombre,
                                                                                                                Ciclo10: ciclo10,
                                                                                                                Nombre10: ciclo10[0].ciclo.nombre,
                                                                                                                nombre: req.user.nom,
                                                                                                                rol: req.user.rolN,
                                                                                                                foto: req.user.foto,
                                                                                                                msg: {
                                                                                                                    error: req.flash('error'),
                                                                                                                    info: req.flash("info")
                                                                                                                }

                                                                                                            });
                                                                                                }
                                                                                            }).catch(function (err) {
                                                                                                req.flash('info', 'Hubo un error');
                                                                                                res.redirect('/');
                                                                                            });
                                                                                        }
                                                                                    }).catch(function (err) {
                                                                                        req.flash('info', 'Hubo un error');
                                                                                        res.redirect('/');
                                                                                    });
                                                                                }
                                                                            }).catch(function (err) {
                                                                                req.flash('info', 'Hubo un error');
                                                                                res.redirect('/');
                                                                            });
                                                                        }
                                                                    }).catch(function (err) {
                                                                        req.flash('info', 'Hubo un error');
                                                                        res.redirect('/');
                                                                    });
                                                                }
                                                            }).catch(function (err) {
                                                                req.flash('info', 'Hubo un error');
                                                                res.redirect('/');
                                                            });
                                                        }
                                                    }).catch(function (err) {
                                                        req.flash('info', 'Hubo un error');
                                                        res.redirect('/');
                                                    });
                                                }
                                            }).catch(function (err) {
                                                req.flash('info', 'Hubo un error');
                                                res.redirect('/');
                                            });
                                        }
                                    }).catch(function (err) {
                                        req.flash('info', 'Hubo un error');
                                        res.redirect('/');
                                    });
                                }
                            }).catch(function (err) {
                                req.flash('info', 'Hubo un error');
                                res.redirect('/');
                            });
                        }
                    }).catch(function (err) {
                        req.flash('info', 'Hubo un error');
                        res.redirect('/');
                    });
                }).catch(function (err) {
                    req.flash('info', 'Hubo un error');
                    res.redirect('/');
                });
            }).catch(function (err) {
                req.flash('info', 'Hubo un error');
                res.redirect('/');
            });
        } else if (req.user.rolN === "Abogado") {
            Carrera.findOne({where: {id: "1"}}).then(function (carrera) {
                Malla.findOne({where: {id: "1"}}).then(function (malla) {

                    Materia.findAll({include: [{model: Silabo, as: "silabo", where: {estado: true}}, {model: Ciclo, where: {id: "1"}}]}).then(function (ciclo1) {
                        if (ciclo1) {
                            Materia.findAll({include: [{model: Silabo, as: "silabo", where: {estado: true}}, {model: Ciclo, where: {id: "2"}}]}).then(function (ciclo2) {
                                if (ciclo2) {
                                    Materia.findAll({include: [{model: Silabo, as: "silabo", where: {estado: true}}, {model: Ciclo, where: {id: "3"}}]}).then(function (ciclo3) {
                                        if (ciclo3) {
                                            Materia.findAll({include: [{model: Silabo, as: "silabo", where: {estado: true}}, {model: Ciclo, where: {id: "4"}}]}).then(function (ciclo4) {
                                                if (ciclo4) {
                                                    Materia.findAll({include: [{model: Silabo, as: "silabo", where: {estado: true}}, {model: Ciclo, where: {id: "5"}}]}).then(function (ciclo5) {
                                                        if (ciclo5) {
                                                            Materia.findAll({include: [{model: Silabo, as: "silabo", where: {estado: true}}, {model: Ciclo, where: {id: "6"}}]}).then(function (ciclo6) {
                                                                if (ciclo6) {
                                                                    Materia.findAll({include: [{model: Silabo, as: "silabo", where: {estado: true}}, {model: Ciclo, where: {id: "7"}}]}).then(function (ciclo7) {
                                                                        if (ciclo7) {
                                                                            Materia.findAll({include: [{model: Silabo, as: "silabo", where: {estado: true}}, {model: Ciclo, where: {id: "8"}}]}).then(function (ciclo8) {
                                                                                if (ciclo8) {
                                                                                    Materia.findAll({include: [{model: Silabo, as: "silabo"}, {model: Ciclo, where: {id: "9"}}]}).then(function (ciclo9) {
                                                                                        if (ciclo9) {
                                                                                            Materia.findAll({include: [{model: Silabo, as: "silabo", where: {estado: true}}, {model: Ciclo, where: {id: "10"}}]}).then(function (ciclo10) {
                                                                                                if (ciclo10) {
                                                                                                    res.render('partials/abogado/index',
                                                                                                            {titulo: "Plan de Estudios",
                                                                                                                partial: 'abogado/frm_plan',
                                                                                                                Carrera: carrera,
                                                                                                                Malla: malla,
                                                                                                                Ciclo1: ciclo1,
                                                                                                                Nombre1: ciclo1[0].ciclo.nombre,
                                                                                                                Ciclo2: ciclo2,
                                                                                                                Nombre2: ciclo2[0].ciclo.nombre,
                                                                                                                Ciclo3: ciclo3,
                                                                                                                Nombre3: ciclo3[0].ciclo.nombre,
                                                                                                                Ciclo4: ciclo4,
                                                                                                                Nombre4: ciclo4[0].ciclo.nombre,
                                                                                                                Ciclo5: ciclo5,
                                                                                                                Nombre5: ciclo5[0].ciclo.nombre,
                                                                                                                Ciclo6: ciclo6,
                                                                                                                Nombre6: ciclo6[0].ciclo.nombre,
                                                                                                                Ciclo7: ciclo7,
                                                                                                                Nombre7: ciclo7[0].ciclo.nombre,
                                                                                                                Ciclo8: ciclo8,
                                                                                                                Nombre8: ciclo8[0].ciclo.nombre,
                                                                                                                Ciclo9: ciclo9,
                                                                                                                Nombre9: ciclo9[0].ciclo.nombre,
                                                                                                                Ciclo10: ciclo10,
                                                                                                                Nombre10: ciclo10[0].ciclo.nombre,
                                                                                                                nombre: req.user.nom,
                                                                                                                rol: req.user.rolN,
                                                                                                                foto: req.user.foto,
                                                                                                                msg: {
                                                                                                                    error: req.flash('error'),
                                                                                                                    info: req.flash("info")
                                                                                                                }

                                                                                                            });
                                                                                                }
                                                                                            }).catch(function (err) {
                                                                                                req.flash('info', 'Hubo un error');
                                                                                                res.redirect('/');
                                                                                            });
                                                                                        }
                                                                                    }).catch(function (err) {
                                                                                        req.flash('info', 'Hubo un error');
                                                                                        res.redirect('/');
                                                                                    });
                                                                                }
                                                                            }).catch(function (err) {
                                                                                req.flash('info', 'Hubo un error');
                                                                                res.redirect('/');
                                                                            });
                                                                        }
                                                                    }).catch(function (err) {
                                                                        req.flash('info', 'Hubo un error');
                                                                        res.redirect('/');
                                                                    });
                                                                }
                                                            }).catch(function (err) {
                                                                req.flash('info', 'Hubo un error');
                                                                res.redirect('/');
                                                            });
                                                        }
                                                    }).catch(function (err) {
                                                        req.flash('info', 'Hubo un error');
                                                        res.redirect('/');
                                                    });
                                                }
                                            }).catch(function (err) {
                                                req.flash('info', 'Hubo un error');
                                                res.redirect('/');
                                            });
                                        }
                                    }).catch(function (err) {
                                        req.flash('info', 'Hubo un error');
                                        res.redirect('/');
                                    });
                                }
                            }).catch(function (err) {
                                req.flash('info', 'Hubo un error');
                                res.redirect('/');
                            });
                        }
                    }).catch(function (err) {
                        req.flash('info', 'Hubo un error');
                        res.redirect('/');
                    });
                }).catch(function (err) {
                    req.flash('info', 'Hubo un error');
                    res.redirect('/');
                });
            }).catch(function (err) {
                req.flash('info', 'Hubo un error');
                res.redirect('/');
            });
        } else if (req.user.rolN === "Docente") {
            Carrera.findOne({where: {id: "1"}}).then(function (carrera) {
                Malla.findOne({where: {id: "1"}}).then(function (malla) {

                    Materia.findAll({include: [{model: Silabo, as: "silabo", where: {estado: true}}, {model: Ciclo, where: {id: "1"}}]}).then(function (ciclo1) {
                        if (ciclo1) {
                            Materia.findAll({include: [{model: Silabo, as: "silabo", where: {estado: true}}, {model: Ciclo, where: {id: "2"}}]}).then(function (ciclo2) {
                                if (ciclo2) {
                                    Materia.findAll({include: [{model: Silabo, as: "silabo", where: {estado: true}}, {model: Ciclo, where: {id: "3"}}]}).then(function (ciclo3) {
                                        if (ciclo3) {
                                            Materia.findAll({include: [{model: Silabo, as: "silabo", where: {estado: true}}, {model: Ciclo, where: {id: "4"}}]}).then(function (ciclo4) {
                                                if (ciclo4) {
                                                    Materia.findAll({include: [{model: Silabo, as: "silabo", where: {estado: true}}, {model: Ciclo, where: {id: "5"}}]}).then(function (ciclo5) {
                                                        if (ciclo5) {
                                                            Materia.findAll({include: [{model: Silabo, as: "silabo", where: {estado: true}}, {model: Ciclo, where: {id: "6"}}]}).then(function (ciclo6) {
                                                                if (ciclo6) {
                                                                    Materia.findAll({include: [{model: Silabo, as: "silabo", where: {estado: true}}, {model: Ciclo, where: {id: "7"}}]}).then(function (ciclo7) {
                                                                        if (ciclo7) {
                                                                            Materia.findAll({include: [{model: Silabo, as: "silabo", where: {estado: true}}, {model: Ciclo, where: {id: "8"}}]}).then(function (ciclo8) {
                                                                                if (ciclo8) {
                                                                                    Materia.findAll({include: [{model: Silabo, as: "silabo"}, {model: Ciclo, where: {id: "9"}}]}).then(function (ciclo9) {
                                                                                        if (ciclo9) {
                                                                                            Materia.findAll({include: [{model: Silabo, as: "silabo", where: {estado: true}}, {model: Ciclo, where: {id: "10"}}]}).then(function (ciclo10) {
                                                                                                if (ciclo10) {
                                                                                                    res.render('partials/docente/index',
                                                                                                            {titulo: "Plan de Estudios",
                                                                                                                partial: 'docente/frm_plan',
                                                                                                                Carrera: carrera,
                                                                                                                Malla: malla,
                                                                                                                Ciclo1: ciclo1,
                                                                                                                Nombre1: ciclo1[0].ciclo.nombre,
                                                                                                                Ciclo2: ciclo2,
                                                                                                                Nombre2: ciclo2[0].ciclo.nombre,
                                                                                                                Ciclo3: ciclo3,
                                                                                                                Nombre3: ciclo3[0].ciclo.nombre,
                                                                                                                Ciclo4: ciclo4,
                                                                                                                Nombre4: ciclo4[0].ciclo.nombre,
                                                                                                                Ciclo5: ciclo5,
                                                                                                                Nombre5: ciclo5[0].ciclo.nombre,
                                                                                                                Ciclo6: ciclo6,
                                                                                                                Nombre6: ciclo6[0].ciclo.nombre,
                                                                                                                Ciclo7: ciclo7,
                                                                                                                Nombre7: ciclo7[0].ciclo.nombre,
                                                                                                                Ciclo8: ciclo8,
                                                                                                                Nombre8: ciclo8[0].ciclo.nombre,
                                                                                                                Ciclo9: ciclo9,
                                                                                                                Nombre9: ciclo9[0].ciclo.nombre,
                                                                                                                Ciclo10: ciclo10,
                                                                                                                Nombre10: ciclo10[0].ciclo.nombre,
                                                                                                                nombre: req.user.nom,
                                                                                                                rol: req.user.rolN,
                                                                                                                foto: req.user.foto,
                                                                                                                msg: {
                                                                                                                    error: req.flash('error'),
                                                                                                                    info: req.flash("info")
                                                                                                                }

                                                                                                            });
                                                                                                }
                                                                                            }).catch(function (err) {
                                                                                                req.flash('info', 'Hubo un error');
                                                                                                res.redirect('/');
                                                                                            });
                                                                                        }
                                                                                    }).catch(function (err) {
                                                                                        req.flash('info', 'Hubo un error');
                                                                                        res.redirect('/');
                                                                                    });
                                                                                }
                                                                            }).catch(function (err) {
                                                                                req.flash('info', 'Hubo un error');
                                                                                res.redirect('/');
                                                                            });
                                                                        }
                                                                    }).catch(function (err) {
                                                                        req.flash('info', 'Hubo un error');
                                                                        res.redirect('/');
                                                                    });
                                                                }
                                                            }).catch(function (err) {
                                                                req.flash('info', 'Hubo un error');
                                                                res.redirect('/');
                                                            });
                                                        }
                                                    }).catch(function (err) {
                                                        req.flash('info', 'Hubo un error');
                                                        res.redirect('/');
                                                    });
                                                }
                                            }).catch(function (err) {
                                                req.flash('info', 'Hubo un error');
                                                res.redirect('/');
                                            });
                                        }
                                    }).catch(function (err) {
                                        req.flash('info', 'Hubo un error');
                                        res.redirect('/');
                                    });
                                }
                            }).catch(function (err) {
                                req.flash('info', 'Hubo un error');
                                res.redirect('/');
                            });
                        }
                    }).catch(function (err) {
                        req.flash('info', 'Hubo un error');
                        res.redirect('/');
                    });
                }).catch(function (err) {
                    req.flash('info', 'Hubo un error');
                    res.redirect('/');
                });
            }).catch(function (err) {
                req.flash('info', 'Hubo un error');
                res.redirect('/');
            });
        } else if (req.user.rolN === "Solicitante") {
            res.redirect('/perfil');
        }
    }

}


module.exports = controladorPlanEstudios;
