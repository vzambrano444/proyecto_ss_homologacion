'use strict';
var models = require('../models/');
var Malla = models.mallaCurricular;
var Carrera = models.carrera;
/**
 * Clase que permite manipular los datos del modelo malla, carrera
 */
class controladorMalla {
    /**
     * Funcion que permite mostrar la vista para visualizar llas mallas academicas
     * @param {type} req el objeto peticion
     * @param {type} res el objeto respuesta
     * @returns {undefined} redirecciona a la pagina /menu/malla
     */
    VisualizarMalla(req, res) {
        if (req.user.rolN === "Decano") {
            Carrera.findAll({}).then(function (carrera) {
                if (carrera) {
                    Malla.findAll({include: {model: Carrera}}).then(function (malla) {
                        if (malla) {
                            res.render('partials/decano/index',
                                    {titulo: "Malla Curricular",
                                        partial: 'decano/frm_malla',
                                        Carrera: carrera,
                                        Malla: malla,
                                        nombre: req.user.nom,
                                        rol: req.user.rolN,
                                        foto: req.user.foto,
                                        msg: {
                                            error: req.flash('error'),
                                            info: req.flash("info")
                                        }
                                    });
                        }
                    }).catch(function (err) {
                        req.flash('info', 'Hubo un error');
                        res.redirect('/');
                    });
                }
            }).catch(function (err) {
                req.flash('info', 'Hubo un error');
                res.redirect('/');
            });
        } else if (req.user.rolN === "Abogado") {
            Carrera.findOne({}).then(function (carrera) {
                if (carrera) {
                    Malla.findOne({include: {model: Carrera}}).then(function (malla) {
                        if (malla) {
                            //  res.send(malla);
                            res.render('partials/abogado/index',
                                    {titulo: "Malla Curricular",
                                        partial: 'abogado/frm_malla',
                                        Carrera: carrera,
                                        Malla: malla,
                                        nombre: req.user.nom,
                                        rol: req.user.rolN,
                                        foto: req.user.foto,
                                        msg: {
                                            error: req.flash('error'),
                                            info: req.flash("info")
                                        }
                                    });
                        }
                    }).catch(function (err) {
                        req.flash('info', 'Hubo un error');
                        res.redirect('/');
                    });
                }
            }).catch(function (err) {
                req.flash('info', 'Hubo un error');
                res.redirect('/');
            });
        } else if (req.user.rolN === "Docente") {
            Carrera.findOne({}).then(function (carrera) {
                if (carrera) {
                    Malla.findOne({include: {model: Carrera}}).then(function (malla) {
                        if (malla) {
                            //  res.send(malla);
                            res.render('partials/docente/index',
                                    {titulo: "Malla Curricular",
                                        partial: 'docente/frm_malla',
                                        Carrera: carrera,
                                        Malla: malla,
                                        nombre: req.user.nom,
                                        rol: req.user.rolN,
                                        foto: req.user.foto,
                                        msg: {
                                            error: req.flash('error'),
                                            info: req.flash("info")
                                        }
                                    });
                        }
                    }).catch(function (err) {
                        req.flash('info', 'Hubo un error');
                        res.redirect('/');
                    });
                }
            }).catch(function (err) {
                req.flash('info', 'Hubo un error');
                res.redirect('/');
            });
        } else if (req.user.rolN === "Solicitante") {
            Carrera.findOne({}).then(function (carrera) {
                Malla.findOne({where: {estado: true}, include: {model: Carrera}}).then(function (malla) {
                    res.render('partials/solicitante/index',
                            {titulo: "Malla Curricular",
                                partial: 'solicitante/frm_malla',
                                Carrera: carrera,
                                Malla: malla,
                                nombre: req.user.nom,
                                rol: req.user.rolN,
                                foto: req.user.foto,
                                msg: {
                                    error: req.flash('error'),
                                    info: req.flash("info")
                                }

                            });
                }).catch(function (err) {
                    req.flash('info', 'Hubo un error');
                    res.redirect('/');
                });
            }).catch(function (err) {
                req.flash('info', 'Hubo un error');
                res.redirect('/');
            });
        }
    }

}


module.exports = controladorMalla;
