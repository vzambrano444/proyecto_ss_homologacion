//load bcrypt
var bCrypt = require('bcrypt-nodejs');
const uuidv4 = require('uuid/v4');
var model = require('../models/');
var cuenta = model.cuenta;
var persona = model.persona;
var rol = model.rol;
module.exports = function (passport) {

    var Cuenta = cuenta; // modelo
    var Persona = persona; // modelo
    var Rol = rol;
    var LocalStrategy = require('passport-local').Strategy;
    //serialize
    passport.serializeUser(function (cuenta, done) {
        done(null, cuenta.id);
    });
    // deserialize user 



    passport.deserializeUser(function (id, done) {
         console.log("//////////////// id-Cuenta =  "+id + "    ///////////////////////////");
        Cuenta.findOne({where: {id: id}, include: [{model: Persona, include: {model: Rol}}]}).then(function (cuenta) {
           
            if (cuenta) {
                var userinfo = {
                    idCuenta: cuenta.id,
                    correo: cuenta.correo,
                    id_cuenta: cuenta.external_id,
                    id_persona: cuenta.persona.external_id,
                    idPersona: cuenta.persona.id,
                    nombre: cuenta.persona.apellidos + " " + cuenta.persona.nombres,
                    rol: cuenta.persona.rol.id,
                    rolN: cuenta.persona.rol.nombre,
                    foto: cuenta.persona.foto,
                    nom: cuenta.persona.nombres,
                };
                console.log("=============DESERIALIZANDO=====================");
                console.log(userinfo.rolN + '  ' + userinfo.nom +'   '+ userinfo.rol);
                console.log("================================================");
                done(null, userinfo);
            } else {
                done(cuenta.errors, null);
            }
        });
    });

    //registro de usuarios por passport
    passport.use('local-signup', new LocalStrategy(
            {
                usernameField: 'correo',
                passwordField: 'clave',
                passReqToCallback: true // allows us to pass back the entire request to the callback
            },
            function (req, correo, clave, done) {
                var generateHash = function (clave) {
                    return bCrypt.hashSync(clave, bCrypt.genSaltSync(8), null);
                };
                //verificar si el email no esta registrado
                Cuenta.findOne({
                    where: {
                        correo: correo
                    }
                }).then(function (cuenta) {
                    if (cuenta) {
                        return done(null, false, {
                            //    message: req.flash('error_registro', 'El correo ya esta registrado')
                        });
                    } else {
                        var userPassword = generateHash(clave);
                        Rol.findOne({
                            where: {nombre: 'Solicitante'}
                        }).then(function (rol) {
                            if (rol) {
                                var dataPersona =
                                        {
                                            cedula: req.body.cedula,
                                            apellidos: req.body.apellidos,
                                            nombres: req.body.nombres,
                                            fecha_nacimiento: req.body.fecha_nacimiento,
                                            edad: req.body.edad,
                                            direccion: req.body.direccion,
                                            telefono: req.body.telefono,
                                            foto: 'sin_foto.jpg',                                           
                                            external_id: uuidv4(),
                                            id_rol: "4",

                                        };
                                Persona.create(dataPersona).then(function (newPersona, created) {
                                    if (!newPersona) {
                                        return done(null, true, {
                                              msg:  req.flash('error', 'La Cuenta no se puede Crear')
                                        });
                                    }
                                    if (newPersona) {
                                        var dataCuenta = {
                                            correo: correo,
                                            clave: userPassword,
                                            id_persona: newPersona.id,
                                            estado: true,
                                            external_id: uuidv4()
                                        };
                                        Cuenta.create(dataCuenta).then(function (newCuenta, created) {
                                            if (newCuenta) {
                                                
                                                return done(null, newCuenta,{
                                                    msg:req.flash('info', 'Se regstro con exito')
                                                });
                                            }
                                            if (!newCuenta) {
                                                console.log("__________cuenta no se pudo crear__________");
                                                //borrar persona
                                                return done(null, false, {
                                                        msg: req.flash('error', 'La Cuenta no se puedo Crear')
                                                });
                                            }

                                        });
                                    }
                                });
                            } else {
                                console.log('__________Rol No Existe__________');
                                return done(null, false, {
                                    //  message: req.flash('error_registro', 'Rol no existe')
                                });
                            }
                        });
                    }
                });
            }
    ));

    //INICIO DE SESIÓN

    passport.use('local-signin', new LocalStrategy(
            {
                usernameField: 'correo',
                passwordField: 'clave',
                passReqToCallback: true // allows us to pass back the entire request to the callback
            },
            function (req, email, password, done) {
                var isValidPassword = function (userpass, password) {
                    return bCrypt.compareSync(password, userpass);
                };
                Cuenta.findOne({where: {correo: email}}).then(function (cuenta) {

                    if (!cuenta) {
                        console.log('__________CUENTA NO EXISTE__________');
                        return done(null, false, {msg: req.flash('error', 'Cuenta no existe')});
                    }

                    if (!isValidPassword(cuenta.clave, password)) {

                        console.log('__________CLAVE INCONRRECTA-___________')
                        return done(null, false, {msg: req.flash('error', 'Clave incorrecta')});
                    }

                    var userinfo = cuenta.get();
                    return done(null, userinfo);
                }).catch(function (err) {
                    console.log("Error:", err);
                    return done(null, false, {msg:req.flash('error', 'Cuenta erronea')});
                });
            }
    ));
};