'use strict';
module.exports = (sequelize, DataTypes) => {
    const mallaCurricular = sequelize.define('mallaCurricular', {
        archivo: DataTypes.STRING,
        anio: DataTypes.INTEGER,
        estado: DataTypes.BOOLEAN,
        external_id: DataTypes.UUID
    }, {freezeTableName: true});

    mallaCurricular.associate = function (models) {
        // associations can be defined here
        mallaCurricular.belongsTo(models.carrera, {foreignKey: 'id_carrera'});
        mallaCurricular.hasMany(models.ciclo, {foreignKey: 'id_mallaCurricular', as: 'ciclo'});
    };

    return mallaCurricular;
};


