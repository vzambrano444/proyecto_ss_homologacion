'use strict';
module.exports = (sequelize, DataTypes) => {
    const seguimiento = sequelize.define('seguimiento', {
        revSolicitud: DataTypes.BOOLEAN,
        revSellos: DataTypes.BOOLEAN,
        asigDocente: DataTypes.BOOLEAN,
        revDocumentacion: DataTypes.BOOLEAN,
        external_id: DataTypes.UUID
    }, {freezeTableName: true});

    seguimiento.associate = function (models) {
        // associations can be defined here
        seguimiento.belongsTo(models.tramite, {foreignKey: 'id_tramite'});
    };

    return seguimiento;
};
