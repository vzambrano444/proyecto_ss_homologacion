'use strict';
module.exports = (sequelize, DataTypes) => {
    const periodoAcademico = sequelize.define('periodoAcademico', {
        fechaInicio: DataTypes.DATEONLY,
        fechaFin: DataTypes.DATEONLY,
        estado: DataTypes.BOOLEAN,
        external_id: DataTypes.UUID,
        external_malla: DataTypes.STRING
    }, {freezeTableName: true});

    periodoAcademico.associate = function (models) {
        // associations can be defined here
        periodoAcademico.hasOne(models.silabo, {foreignKey: 'id_periodoAcademico', as: 'silabo'});
    };

    return periodoAcademico;
};

