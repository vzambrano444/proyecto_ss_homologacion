'use strict';
module.exports = (sequelize, DataTypes) => {
    const silabo = sequelize.define('silabo', {
        estado: DataTypes.BOOLEAN,
        archivo: DataTypes.STRING,
        external_id: DataTypes.UUID
    }, {freezeTableName: true});

    silabo.associate = function (models) {
        // associations can be defined here
        silabo.belongsTo(models.docente, {foreignKey: 'id_docente'});
        silabo.belongsTo(models.periodoAcademico, {foreignKey: 'id_periodoAcademico'});
        silabo.belongsTo(models.materia, {foreignKey: 'id_materia'});
    };

    return silabo;
};

